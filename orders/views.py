import datetime

from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from .models import OrderItem
from .forms import OrderCreateForm
from cart.cart import Cart
from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import get_object_or_404
from .models import Order
from django.http import FileResponse
from django.views import View
from reportlab.lib.pagesizes import letter
from reportlab.pdfgen import canvas
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from io import BytesIO


@login_required
def order_create(request):
    cart = Cart(request)
    if request.method == 'POST':
        form = OrderCreateForm(request.POST)
        if form.is_valid():
            # order = form.save()
            order = form.save(commit=False)
            if cart.coupon:
                order.coupon = cart.coupon
                order.discount = cart.coupon.discount
            order.save()
            for item in cart:
                OrderItem.objects.create(order=order,
                                         product=item['product'],
                                         price=item['price'],
                                         quantity=item['quantity'])
            # очистка корзины
            cart.clear()
            return render(request, 'orders/order_created.html',
                          {'order': order})
    else:
        form = OrderCreateForm
    return render(request, 'orders/order_create.html',
                  {'cart': cart, 'form': form})


@staff_member_required
def admin_order_detail(request, order_id):
    order = get_object_or_404(Order, id=order_id)
    return render(request,
                  'admin/orders/order/detail.html',
                  {'order': order})


class PDFInvoiceView(View):

    def get(self, request, order_id):
        pdfmetrics.registerFont(TTFont('ArialUnicodeMS', 'static/fonts/ARIALUNI.TTF'))
        pdfmetrics.registerFont(TTFont('ArialUnicodeMS-Bold', 'static/fonts/Arial-Unicode-Bold.ttf'))
        order = get_object_or_404(Order, id=order_id)

        # Создаем буфер для PDF
        buffer = BytesIO()

        # Создаем объект Canvas
        p = canvas.Canvas(buffer, pagesize=letter)

        # Настраиваем шрифты и стиль текста
        p.setFont("ArialUnicodeMS-Bold", 16)

        # Генерируем содержимое PDF
        p.drawString(250, 750, "Заказ №" + str(order.id))

        p.setFont("ArialUnicodeMS-Bold", 12)
        p.drawString(60, 730, "Купленные товары")

        p.setFont("ArialUnicodeMS-Bold", 10)

        p.drawString(50, 700, "Наменование ПРОДУКТ")
        p.drawString(320, 700, "ЦЕНА")
        p.drawString(410, 700, "КОЛ-ВО")
        p.drawString(500, 700, "Сумма")

        p.setFont("ArialUnicodeMS", 10)
        # Данные таблицы
        data = []
        y = 660
        for product in OrderItem.objects.filter(order=order.id):
            data.append(
                [product.product.product_name, str(product.price), str(product.quantity), str(product.get_cost())], )

            if len(product.product.product_name) > 45:
                p.drawString(50, y, product.product.product_name[:45] + '......')
            else:
                p.drawString(50, y, product.product.product_name)
            p.drawString(320, y, str(product.price))
            p.drawString(420, y, str(product.quantity))
            p.drawString(500, y, str(product.get_cost()))
            y += 20

        if order.coupon:
            p.drawString(70, 620, 'Промежуточный итог')
            p.drawString(470, 620, f"{round(order.get_total_sum(), 2)}")

            p.drawString(70, 600, f'СКИДКА: Купон "{order.coupon.code} (скидка {order.discount}%)"')
            p.drawString(470, 600, f"-{round(order.get_total_discount(), 2)}")

        p.setFont("ArialUnicodeMS-Bold", 12)
        p.drawString(50, 580, "Итого")
        p.drawString(470, 580, f"{round(order.get_total_cost(), 2)} тенге")

        p.setFont("ArialUnicodeMS", 12)
        p.drawString(70, 500, f"Созданный           {order.created}")
        p.drawString(70, 480, f"Клиент                  {order.first_name} {order.last_name}")
        p.drawString(70, 460, f"E-mail                    {order.email}")
        p.drawString(70, 440, f"Адрес                    {order.city}")
        p.drawString(70, 420, f"Общая сумма       {round(order.get_total_cost(), 2)} тенге")

        # Закрываем объект Canvas
        p.showPage()
        p.save()

        # Возвращаем PDF как HTTP-ответ
        buffer.seek(0)
        response = FileResponse(buffer, as_attachment=True,
                                filename=f"order_{order.id}date_{datetime.datetime.now().day}-{datetime.datetime.now().month}-{datetime.datetime.now().year}_{datetime.datetime.now().hour}-{datetime.datetime.now().minute}_invoice.pdf")
        return response
