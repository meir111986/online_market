from django.urls import path
from api_client.views import CommentsAPICreateView, CommentsAPIList

urlpatterns = [
    path('create/', CommentsAPICreateView.as_view(), name='create_comment'),
    path('comments/', CommentsAPIList.as_view()),
]
