from django.contrib.auth import get_user_model
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from api_client.models import Comments


class CommentsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Comments
        fields = ['id', 'advantages', 'flaws', 'text', 'author', 'product', 'rating']
        read_only_fields = ['id']

        def clean(self):

            if not self.fields[1] and not self.fields[2]:
                raise ValidationError("Укажи хоть что-то")


class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        read_only_fields = ['id']
        exclude = ['password', 'groups', 'user_permissions']


class CommentsListViewSerializer(serializers.ModelSerializer):
    author = AuthorSerializer(many=False, read_only=True)

    class Meta:
        model = Comments
        fields = '__all__'
        read_only_fields = ['id']
