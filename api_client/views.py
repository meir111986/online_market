from rest_framework import status
from rest_framework.generics import ListAPIView
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.views import APIView
from api_client.models import Comments
from api_client.permissions import IsAuthenticatedOrReadOnly
from api_client.serializers import CommentsSerializer, CommentsListViewSerializer


class CommentsAPICreateView(APIView):
    """
        API_View для создания записи 'Комментария' с помощью JS
    """
    queryset = Comments.objects.all()
    serializer_class = CommentsSerializer

    def get(self, request, *args, **kwargs):
        serializer = self.serializer_class(self.queryset.all(), many=True)
        return Response(data=serializer.data)

    def post(self, request, *args, **kwargs):
        if not self.request.data['advantages'] and not self.request.data['flaws'] and not self.request.data['text']:
            return Response({'error': "Что нибудь заполните"})
        serializer = self.serializer_class(data=self.request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(data=serializer.data, status=status.HTTP_201_CREATED)


class CommentsAPIListPagination(PageNumberPagination):
    page_size = 5
    page_size_query_param = 'page_size'
    max_page_size = 1000


class CommentsAPIList(ListAPIView):
    serializer_class = CommentsListViewSerializer
    pagination_class = CommentsAPIListPagination
    permission_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self):
        product_id = self.request.query_params.get('product')
        queryset = Comments.objects.filter(product=product_id).order_by('-id')
        return queryset

