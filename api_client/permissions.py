from rest_framework import permissions


class IsAuthenticatedOrReadOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        # Разрешить GET-запросы всем
        if request.method in permissions.SAFE_METHODS:
            return True
        # Разрешить остальные запросы только аутентифицированным пользователям
        return request.user and request.user.is_authenticated
