from django.contrib.auth import get_user_model
from django.db import models

from market.models import Product


class Comments(models.Model):
    author = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
        related_name='user_comments',
        verbose_name='Автор'
    )
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='product_comments', verbose_name='Продукт')
    advantages = models.TextField(max_length=6000, verbose_name='Достоинства', default=None, blank=True, null=True)
    flaws = models.TextField(max_length=6000, verbose_name='Недостатки', default=None, blank=True, null=True)
    text = models.TextField(max_length=6000, verbose_name='Текст отзыва', blank=True, null=True)
    rating = models.IntegerField(verbose_name='Рейтинг')
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name="Время создания", blank=True)
    updated_at = models.DateTimeField(
        auto_now=True, verbose_name="Время изменения", blank=True)

    def __str__(self):
        return f"{self.author}. {self.text}"

    class Meta:
        verbose_name = 'Коментария'
        verbose_name_plural = 'Коментарии'
