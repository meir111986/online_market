# Generated by Django 4.2.4 on 2023-10-24 17:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('market', '0037_ramcharacteristic_product_name_copy'),
    ]

    operations = [
        migrations.AddField(
            model_name='diskdrivescharacteristic',
            name='product_name_copy',
            field=models.CharField(blank=True, max_length=500, null=True, verbose_name='Наименование товара'),
        ),
    ]
