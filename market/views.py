from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.core.paginator import Paginator
from django.db.models import Q
from django.http import HttpRequest
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from django.views import View
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView

from api_client.models import Comments
from cart.forms import CartAddProductForm
from .colors import COLOR_CHOICES
from .forms import (ProductForm, BatteryCharacteristicForm, CableCharacteristicForm, ProductUpdateForm,
                    ExternalBatteryCharacteristicForm, PhoneHoldersCharacteristicForm,
                    SmartphoneDisplaysCharacteristicForm, ChargingDeviceCharacteristicForm,
                    ProtectiveFilmsGlassesCharacteristicForm, StickersPhonesCharacteristicForm,
                    SmartphoneCasesCharacteristicForm, ProtectiveFilmsGlassesSmartFitnessBraceletsCharacteristicForm,
                    SmartWatchCharacteristicForm, FitnessBraceletsCharacteristicForm, CFSWFBCharacteristicForm,
                    StrapsSmartWatchesCharacteristicForm, CellPhonesCharacteristicForm,
                    SatellitePhonesCharacteristicForm, RAMCharacteristicForm, DiskDrivesCharacteristicForm,
                    VideoCardsCharacteristicForm, ProcessorsCharacteristicForm, SystemUnitsCharacteristicForm,
                    MonoblocksCharacteristicForm)
from .models import (Category, Product, BatteryCharacteristic, CableCharacteristic, Brand, ProductAttachment,
                     ExternalBatteryCharacteristic, PhoneHoldersCharacteristic, SmartphoneDisplaysCharacteristic,
                     ChargingDeviceCharacteristic, ProtectiveFilmsGlassesCharacteristic, StickersPhonesCharacteristic,
                     SmartphoneCasesCharacteristic, ProtectiveFilmsGlassesSmartFitnessBraceletsCharacteristic,
                     StrapsSmartWatchesCharacteristic, SmartWatchCharacteristic, FitnessBraceletsCharacteristic,
                     CFSWFBCharacteristic, CellPhonesCharacteristic, SatellitePhonesCharacteristic, RAMCharacteristic,
                     DiskDrivesCharacteristic, VideoCardsCharacteristic, ProcessorsCharacteristic,
                     SystemUnitsCharacteristic, MonoblocksCharacteristic)


class ProductPriceBrand:
    def get_price(self):
        return Product.objects.all()

    def get_brand(self):
        return Brand.objects.all().order_by('brand_name')


class CategoryModelForm(View):
    def get_category_model_form(self):
        models = {
            16: {BatteryCharacteristic: BatteryCharacteristicForm},
            13: {CableCharacteristic: CableCharacteristicForm},
            12: {ExternalBatteryCharacteristic: ExternalBatteryCharacteristicForm},
            11: {PhoneHoldersCharacteristic: PhoneHoldersCharacteristicForm},
            18: {SmartphoneDisplaysCharacteristic: SmartphoneDisplaysCharacteristicForm},
            9: {ChargingDeviceCharacteristic: ChargingDeviceCharacteristicForm},
            10: {ProtectiveFilmsGlassesCharacteristic: ProtectiveFilmsGlassesCharacteristicForm},
            20: {StickersPhonesCharacteristic: StickersPhonesCharacteristicForm},
            8: {SmartphoneCasesCharacteristic: SmartphoneCasesCharacteristicForm},
            26: {ProtectiveFilmsGlassesSmartFitnessBraceletsCharacteristic: ProtectiveFilmsGlassesSmartFitnessBraceletsCharacteristicForm},
            22: {StrapsSmartWatchesCharacteristic: StrapsSmartWatchesCharacteristicForm},
            21: {SmartWatchCharacteristic: SmartWatchCharacteristicForm},
            23: {FitnessBraceletsCharacteristic: FitnessBraceletsCharacteristicForm},
            25: {CFSWFBCharacteristic: CFSWFBCharacteristicForm},
            5: {CellPhonesCharacteristic: CellPhonesCharacteristicForm},
            7: {SatellitePhonesCharacteristic: SatellitePhonesCharacteristicForm},
            35: {RAMCharacteristic: RAMCharacteristicForm},
            36: {DiskDrivesCharacteristic: DiskDrivesCharacteristicForm},
            37: {VideoCardsCharacteristic: VideoCardsCharacteristicForm},
            38: {ProcessorsCharacteristic: ProcessorsCharacteristicForm},
            40: {SystemUnitsCharacteristic: SystemUnitsCharacteristicForm},
            41: {MonoblocksCharacteristic: MonoblocksCharacteristicForm},
        }
        return models

    def get_characteristic_model(self):
        for category_id, model_form in self.get_category_model_form().items():
            for model, form in model_form.items():
                if model.objects.filter(product=self.kwargs['pk']):
                    return model

    def get_create_characteristic_form(self, data=None):
        for category_id, model_form in self.get_category_model_form().items():
            for model, form in model_form.items():
                if self.kwargs['catid'] == category_id:
                    return form(data=data)

    def get_edit_characteristic_form(self, data=None):
        for category_id, model_form in self.get_category_model_form().items():
            for model, form in model_form.items():
                if model.objects.filter(product=self.kwargs['pk']):
                    product = model.objects.get(product=self.kwargs['pk'])
                    return form(instance=product, data=data)


class CategoryListView(ListView):
    model = Category
    template_name = "category/category_list.html"
    queryset = model.objects.filter(parent_id=None).exclude(slug='all-products').order_by('-title')

    def get(self, request, *args, **kwargs):
        if request.GET.get('search') is None or request.GET.get('search') == '':
            return super().get(request, *args, **kwargs)
        search_param = request.GET.get('search')
        result = Product.objects.filter(
            Q(product_name__icontains=search_param) | Q(article__icontains=search_param)
            | Q(category__title__icontains=search_param) | Q(brand__brand_name__icontains=search_param),
        ).order_by('updated_at')
        return render(request, 'product/product_list_search_view.html', {'products': result})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        category_counts = {}
        for category in Category.objects.all():
            count = category.get_product_count()
            category_counts[category.title] = count
        context['category_counts'] = dict(sorted(category_counts.items(), key=lambda item: item[1], reverse=True))
        return context


class ProductListView(ProductPriceBrand, ListView):
    model = Product
    template_name = 'product/product_list_view.html'
    context_object_name = 'products'
    paginate_by = 6

    def get(self, request, *args, **kwargs):
        if request.GET.get('search') is None or request.GET.get('search') == '':
            return super().get(request, *args, **kwargs)
        search_param = request.GET.get('search')
        result = Product.objects.filter(
            Q(product_name__icontains=search_param) | Q(article__icontains=search_param)
            | Q(category__title__icontains=search_param) | Q(brand__brand_name__icontains=search_param),
        ).order_by('updated_at')
        return render(request, 'product/product_list_search_view.html', {'products': result})

    def get_queryset(self):
        self.category = get_object_or_404(Category, slug=self.kwargs['slug'])
        category_parent_child = Category.objects.get(id=self.category.pk)
        self.descendants = category_parent_child.get_descendants(include_self=True)
        self.parent_elements = category_parent_child.get_ancestors(include_self=True)
        queryset = Product.objects.filter(category__in=self.descendants).order_by('-views_count')
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = self.category
        context['slag_product'] = self.category
        context['object_list'] = self.descendants
        context['parent_elements'] = self.parent_elements
        category_counts = {}
        for category in Category.objects.all():
            count = category.get_product_count()
            category_counts[category.title] = count
        context['category_counts'] = category_counts
        return context


class UserProductListView(LoginRequiredMixin, ListView):
    model = Product
    template_name = 'product/user_product_list_view.html'
    context_object_name = 'products'
    paginate_by = 8

    def get(self, request, *args, **kwargs):
        if request.GET.get('search') is None or request.GET.get('search') == '':
            return super().get(request, *args, **kwargs)
        search_param = request.GET.get('search')
        result = Product.objects.filter(
            Q(product_name__icontains=search_param, user=request.user) | Q(article__icontains=search_param, user=request.user)
            | Q(category__title__icontains=search_param, user=request.user) | Q(brand__brand_name__icontains=search_param, user=request.user),
        ).order_by('updated_at')
        return render(request, 'product/user_product_list_view.html', {'products': result})

    def get_queryset(self):
        queryset = Product.objects.filter(user=self.request.user).order_by('-updated_at')
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        category_counts = {}
        for category in Category.objects.all():
            count = category.get_product_count()
            category_counts[category.title] = count
        context['category_counts'] = category_counts
        return context


class FilterProductPrice(ProductPriceBrand, ListView):
    model = Product
    template_name = 'product/product_list_view.html'
    context_object_name = 'products'

    def get_queryset(self):
        self.category = get_object_or_404(Category, slug=self.kwargs['slug'])
        price_list = self.request.GET.getlist("price")
        brand_list = self.request.GET.getlist("brand")
        search_param = self.request.GET.get('search')
        if self.request.GET == {}:
            return Product.objects.filter(category_id=self.category.id)

        price_array = None
        if self.request.GET.get("price1"):
            price_array = list(range(1, 600))
        elif self.request.GET.get("price2"):
            price_array = list(range(600, 1400))
        elif self.request.GET.get("price3"):
            price_array = list(range(1400, 5000))
        if self.request.GET.get("price1") and self.request.GET.get("price2"):
            price_array = list(range(1, 600)) + list(range(600, 1400))
        if self.request.GET.get("price1") and self.request.GET.get("price3"):
            price_array = list(range(1, 600)) + list(range(1400, 5000))
        if self.request.GET.get("price2") and self.request.GET.get("price3"):
            price_array = list(range(600, 1400)) + list(range(1400, 5000))
        if self.request.GET.get("price1") and self.request.GET.get("price2") and self.request.GET.get("price3"):
            price_array = list(range(1, 600)) + list(range(600, 1400)) + list(range(1400, 5000))

        if (
                self.request.GET.get("price1") is None and
                self.request.GET.get("price2") is None and
                self.request.GET.get("price3") is None
        ):
            price_array = Product.objects.all()

        if search_param:
            queryset = self.model.objects.filter(
                Q(product_name__icontains=search_param) | Q(article__icontains=search_param)
                | Q(category__title__icontains=search_param) | Q(brand__brand_name__icontains=search_param),
            ).order_by('updated_at')
            return queryset

        for get_request in self.request.GET.keys():
            if get_request in ['price1', 'price2', 'price3'] and not self.request.GET.get("brand"):
                queryset = Product.objects.filter(
                    Q(price__in=price_array, category_id=self.category.id)
                )
                return queryset
            elif get_request == 'brand' and get_request not in ['price1', 'price2', 'price3']:
                queryset = Product.objects.filter(
                    Q(brand__brand_name__in=brand_list, category_id=self.category.id)
                )
                return queryset
            else:
                queryset = Product.objects.filter(
                    Q(price__in=price_array, category_id=self.category.id)
                    & Q(brand__brand_name__in=brand_list, category_id=self.category.id)
                )
                return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = self.category
        context['slag_product'] = self.category
        category_parent_child = Category.objects.get(id=self.category.pk)
        self.descendants = category_parent_child.get_descendants(include_self=True)
        context['object_list'] = self.descendants
        return context


class ProductDetailView(DetailView, CategoryModelForm):
    template_name = 'product/product_detail_view.html'
    model = Product
    context_object_name = 'product'
    paginate_related_by = 5
    paginate_related_orphans = 0

    def get(self, request, *args, **kwargs):
        if request.GET.get('search') is None or request.GET.get('search') == '':
            return super().get(request, *args, **kwargs)
        search_param = request.GET.get('search')
        result = Product.objects.filter(
            Q(product_name__icontains=search_param) | Q(article__icontains=search_param)
            | Q(category__title__icontains=search_param) | Q(brand__brand_name__icontains=search_param),
        ).order_by('updated_at')
        return render(request, 'product/product_list_search_view.html', {'products': result})

    def get_object(self, queryset=None):
        obj = super().get_object(queryset=queryset)
        # Увеличиваем счетчик просмотров
        if not self.request.session.get('viewed_%s' % obj.pk, False): # это метод, который пытается получить значение переменной с ключом 'viewed_идентификатор_объекта' из сессии.
            obj.views_count += 1
            obj.save()
            self.request.session['viewed_%s' % obj.pk] = True
        return obj

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        product = self.get_characteristic_model().objects.get(product=self.kwargs['pk'])
        context['product_information'] = product
        context['product_view_count'] = self.object.views_count
        category_parent_child = Category.objects.get(slug=self.kwargs['slug'])
        parent_elements = category_parent_child.get_ancestors(include_self=True)
        context['parent_elements'] = parent_elements
        fields_with_verbose_names = [
            (field.verbose_name, getattr(product, field.name))
            for field in self.get_characteristic_model()._meta.get_fields()
            if field.concrete
        ]
        context['fields_with_verbose_names'] = fields_with_verbose_names
        context['first_image'] = ProductAttachment.objects.filter(product=self.kwargs['pk']).last()
        context['images'] = ProductAttachment.objects.filter(product=self.kwargs['pk'])
        context['product_count'] = Comments.objects.filter(product=self.kwargs['pk']).count()
        for color in COLOR_CHOICES:
            if color[0] == product.color:
                context['product_information_color'] = color[1]
        context['color_choices'] = COLOR_CHOICES
        context['cart_product_form'] = CartAddProductForm()
        comments = self.object.product_comments.order_by('-id')
        paginator = Paginator(comments, self.paginate_related_by,
                              orphans=self.paginate_related_orphans)
        page_number = self.request.GET.get('page', 1)
        page = paginator.get_page(page_number)
        context['page_obj'] = page
        context['comments'] = page.object_list
        context['is_paginated'] = page.has_other_pages()
        return context


class CategoryCreateProductListView(ListView):
    model = Category
    context_object_name = 'categories'
    template_name = "product/category_create_product_list.html"


class ProductCreateView(LoginRequiredMixin, CreateView, CategoryModelForm):
    template_name = 'product/product_create_view.html'
    model = Product
    form_class = ProductForm

    def post(self, request: HttpRequest, *args, **kwargs):
        category = Category.objects.get(pk=kwargs['catid'])
        form_product = ProductForm(data=self.request.POST)
        attachments = request.FILES.getlist('image')
        form_characteristic = self.get_create_characteristic_form(data=request.POST)
        if form_product.is_valid() and form_characteristic.is_valid():
            product = form_product.save(commit=False)
            product.category = Category.objects.get(pk=kwargs['catid'])
            product.user = request.user
            product_characteristic = form_characteristic.save(commit=False)
            if hasattr(product_characteristic, 'product_name_copy'):
                product_characteristic.product_name_copy = product.product_name
            product.save()
            product_characteristic.save()
            product_characteristic.product.add(product)
            for image in attachments:
                ProductAttachment.objects.create(
                    image=image,
                    product_id=product.pk
                )
            return redirect(reverse('product_detail', kwargs={'pk': product.pk, 'slug': category.slug}))
        else:
            form_product = ProductForm(data=self.request.POST)
            form_characteristic = self.get_create_characteristic_form(data=self.request.POST)
        return render(request, self.template_name, context={
            'form': form_product,
            'form_characteristic': form_characteristic,
        })

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        category = Category.objects.get(pk=self.kwargs['catid'])
        context['catid'] = category
        context['title'] = category.title
        context['form_characteristic'] = self.get_create_characteristic_form()
        return context


class ProductUpdateView(UserPassesTestMixin, UpdateView, CategoryModelForm):
    template_name = 'product/product_update_view.html'
    form_class = ProductUpdateForm
    model = Product
    second_form_class = CableCharacteristicForm
    context_object_name = 'products'
    permission_required = 'market.change_product'

    def post(self, request, *args, **kwargs):
        product_pk = Product.objects.get(pk=kwargs['pk'])
        images = ProductAttachment.objects.filter(product_id=product_pk.pk)
        form_product = self.form_class(data=self.request.POST, instance=product_pk)
        form_characteristic = self.get_edit_characteristic_form(self.request.POST)
        if form_product.is_valid() and form_characteristic.is_valid():
            product = form_product.save(commit=False)
            attachments = request.FILES.getlist('image')
            for image in attachments:
                ProductAttachment.objects.create(
                    image=image,
                    product_id=product.pk
                )
            chosen = request.POST.getlist('attachments')
            chosen = [int(x) for x in chosen]
            ProductAttachment.objects.filter(pk__in=chosen).delete()
            product_characteristic = form_characteristic.save(commit=False)
            product.save()
            product_characteristic.save()

            return redirect(reverse('product_detail', kwargs={'pk': product.pk, 'slug': product.category.slug}))
        return render(request, self.template_name, context={
            'form': form_product,
            'form_characteristic': form_characteristic,
            'images': images,
        })

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        product_pk = Product.objects.get(pk=self.kwargs['pk'])
        context['form_characteristic'] = self.get_edit_characteristic_form()
        images = ProductAttachment.objects.filter(product_id=product_pk.pk)
        context['images'] = images
        return context

    def test_func(self):
        return self.request.user.has_perm(
            self.permission_required) and self.request.user == self.get_object().user


class ProductDeleteView(UserPassesTestMixin, DeleteView, CategoryModelForm):
    model = Product
    success_url = 'product_list'
    permission_required = 'market.delete_product'

    def post(self, request, *args, **kwargs):
        product = get_object_or_404(self.model, pk=kwargs.get('pk'))
        characteristic = self.get_characteristic_model().objects.get(product=self.kwargs['pk'])
        product.delete()
        characteristic.delete()
        return redirect(self.get_success_url())

    def get_success_url(self):
        return reverse(self.success_url, kwargs={'slug': self.kwargs.get('slug')})

    def test_func(self):
        return self.request.user.has_perm(
            self.permission_required) and self.request.user == self.get_object().user

