from django.contrib.auth import get_user_model
from django.db import models
from django.urls import reverse
from django.utils import timezone
from mptt.models import MPTTModel, TreeForeignKey
from django.dispatch import receiver
from market.colors import COLOR_CHOICES
import os
from market.helpers import QUESTION, OPERATING_SYSTEM, MEMORY_TYPE, DISK_TYPE, VIDEO_MEMORY_TYPE


class Category(MPTTModel):
    title = models.CharField(max_length=300, unique=True, verbose_name='Название')
    parent = TreeForeignKey('self', on_delete=models.PROTECT, null=True, blank=True, related_name='children',
                            db_index=True, verbose_name='Родительская категория')
    slug = models.SlugField()
    image = models.ImageField(upload_to='category_images', null=True, blank=True,  verbose_name='Фотография')

    class MPTTMeta:
        order_insertion_by = ['title']

    class Meta:
        unique_together = [['parent', 'slug']]
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def get_absolute_url(self):
        return reverse('product_list', args=[str(self.slug)])

    def __str__(self):
        return f"{self.id}-{self.title}"

    def get_product_count(self):
        product_count = self.products.count()
        for child in self.get_children():
            product_count += child.get_product_count()
        return product_count


class Brand(models.Model):
    brand_name = models.CharField(max_length=200, verbose_name='Наименование бренда', unique=True)
    description = models.TextField(blank=True, null=True, verbose_name='Примечание')

    class Meta:
        verbose_name = 'Бренд'
        verbose_name_plural = 'Бренды'

    def __str__(self):
        return self.brand_name


class Product(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.SET_DEFAULT, default=1,
                             related_name='product_users', verbose_name='Пользователь')
    product_name = models.CharField(max_length=500, verbose_name='Наименование товара')
    price = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Цена товара')
    article = models.CharField(max_length=20, verbose_name='Артикул')
    category = TreeForeignKey(Category, on_delete=models.CASCADE, related_name='products', verbose_name='Категория')
    link_to_youtube = models.URLField(blank=True, null=True, verbose_name='Ссылка на Youtube')
    brand = models.ForeignKey('Brand', on_delete=models.CASCADE, verbose_name='Бренд')
    available = models.BooleanField(default=True, verbose_name='В наличие')
    description = models.TextField(null=True, blank=True, verbose_name='Описание')
    creation_date = models.DateTimeField(default=timezone.now, verbose_name='Дата создание')
    updated_at = models.DateTimeField(auto_now=True, verbose_name="Время изменения", blank=True)
    views_count = models.PositiveIntegerField(default=0, verbose_name="Подсчета просмотров")

    class Meta:
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'

    def __str__(self):
        return f"{self.id}-{self.product_name}; Категория-{self.category}"

    def get_last_image(self):
        try:
            last_image = self.images.last().image
            return last_image
        except AttributeError:
            last_image = self.images.image
            return last_image

    def get_product_category(self):
        if self.category.pk == 16:
            return BatteryCharacteristic
        elif self.category.pk == 13:
            return CableCharacteristic


class ProductAttachment(models.Model):
    image = models.ImageField(upload_to='productimages', verbose_name='Фотография')
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='images', verbose_name='Продукт')

    class Meta:
        verbose_name = 'Изображения'
        verbose_name_plural = 'Изображении'

    def __str__(self) -> str:
        return f'image {self.id} for product {self.product}'


# Используется при удаление всего поста, удаляет картинку
@receiver(models.signals.pre_delete, sender=ProductAttachment)
def auto_delete_file(sender, instance, **kwargs):
    if instance.image:
        if os.path.isfile(instance.image.path):
            os.remove(instance.image.path)


# Удаление фото или картинки
# Используется при изменение картинки, удаляет старую картинку
@receiver(models.signals.pre_save, sender=ProductAttachment)
def auto_change_file(sender, instance, **kwargs):
    if not instance.pk:
        return False

    try:
        old_image = Product.objects.get(pk=instance.pk).image
    except Product.DoesNotExist:
        return False

    new_image = instance.image
    if old_image != new_image:
        if os.path.isfile(old_image.path):
            os.remove(old_image.path)


class BatteryCharacteristic(models.Model):
    product = models.ManyToManyField(Product, related_name='batteries', verbose_name='Продукт')
    type = models.CharField(max_length=200, verbose_name='Тип')
    battery_capacity = models.DecimalField(max_digits=7, decimal_places=2, verbose_name='Емкость аккумулятора')
    voltage = models.DecimalField(max_digits=7, decimal_places=2, verbose_name='Напряжение')
    additional_information = models.CharField(max_length=150, blank=True, null=True,
                                              verbose_name='Дополнительная информация')
    dimensions = models.CharField(max_length=100, blank=True, null=True, verbose_name='Размеры')
    weight = models.IntegerField(blank=True, null=True, verbose_name='Вес')
    color = models.CharField(max_length=100, choices=COLOR_CHOICES, blank=True, null=True, verbose_name='Цвет')

    class Meta:
        verbose_name = 'Аккумулятор'
        verbose_name_plural = 'Аккумуляторы'

    def __str__(self):
        return f"{self.id}-{self.product} - {self.type}"


class CableCharacteristic(models.Model):
    product = models.ManyToManyField(Product, related_name='cabeles', verbose_name='Продукт')
    connectors = models.CharField(max_length=200, verbose_name='Разъемы')
    type = models.CharField(max_length=200, verbose_name='Тип')
    length = models.DecimalField(max_digits=5, decimal_places=2, verbose_name='Длина кабеля')
    color = models.CharField(max_length=100, choices=COLOR_CHOICES, blank=True, null=True, verbose_name='Цвет')

    class Meta:
        verbose_name = 'Кабель'
        verbose_name_plural = 'Кабели'

    def __str__(self):
        return f"{self.id}-{self.product} - {self.type}"


class ExternalBatteryCharacteristic(models.Model):
    product = models.ManyToManyField(Product, related_name='external_battery', verbose_name='Продукт')

    voltage = models.CharField(max_length=200, verbose_name='Напряжение')
    capacity = models.CharField(max_length=200, verbose_name='Емкость')
    maximum_output_current = models.CharField(max_length=200, verbose_name='Максимальный выходной ток')
    exits = models.CharField(max_length=200, verbose_name='Выходы')
    connecting_power_source = models.CharField(max_length=200, verbose_name='Подключение к источнику питания',
                                               blank=True, null=True)
    number_of_USB_ports = models.SmallIntegerField(verbose_name='Число USB портов')
    purpose = models.CharField(max_length=200, verbose_name='Назначение', blank=True, null=True)

    built_in_battery_type = models.CharField(max_length=200, verbose_name='Тип встроенного аккумулятора')
    charger = models.CharField(max_length=200, verbose_name='Зарядка', blank=True, null=True)
    fast_charging_support = models.CharField(max_length=100, choices=QUESTION, verbose_name='Поддержка быстрой зарядки')
    solar_battery = models.CharField(max_length=100, choices=QUESTION, blank=True, null=True,
                                     verbose_name='Солнечная батарея')
    short_circuit_protection = models.CharField(max_length=100, choices=QUESTION, blank=True, null=True,
                                                verbose_name='Защита от короткого замыкания')
    overload_protection = models.CharField(max_length=100, choices=QUESTION, blank=True, null=True,
                                           verbose_name='Защита от перегрузки')
    overheat_protection = models.CharField(max_length=100, choices=QUESTION, blank=True, null=True,
                                           verbose_name='Защита от перегрева')
    wireless_charging_support = models.CharField(max_length=100, choices=QUESTION,
                                                 verbose_name='Поддержка беспроводной зарядки')

    peculiarities = models.CharField(max_length=200, verbose_name='Особенности', blank=True, null=True)
    housing_material = models.CharField(max_length=200, verbose_name='Материал корпуса')
    equipment = models.CharField(max_length=200, verbose_name='Комплектация', blank=True, null=True)
    dimensions = models.CharField(max_length=200, verbose_name='Размеры', blank=True, null=True)
    weight = models.IntegerField(blank=True, null=True, verbose_name='Вес')
    color = models.CharField(max_length=100, choices=COLOR_CHOICES, verbose_name='Цвет')

    class Meta:
        verbose_name = 'Внешнии аккумулятор'
        verbose_name_plural = 'Внешние аккумуляторы'

    def __str__(self):
        return f"{self.id}-{self.product} - {self.voltage}"


class PhoneHoldersCharacteristic(models.Model):
    product = models.ManyToManyField(Product, related_name='phone_holders', verbose_name='Продукт')
    purpose = models.CharField(max_length=200, verbose_name='Назначение')
    type = models.CharField(max_length=200, verbose_name='Тип')
    holder_mounting_location = models.CharField(max_length=200, verbose_name='Место крепления держателя')
    holder_mounting_method = models.CharField(max_length=200, verbose_name='Способ крепления держателя')
    maximum_device_weight = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True, verbose_name='Максимальный вес устройства')
    maximum_device_diagonal = models.DecimalField(max_digits=5, decimal_places=2, verbose_name='Максимальная диагональ устройства')
    housing_material = models.CharField(max_length=200, verbose_name='Материал')
    peculiarities = models.CharField(max_length=200, verbose_name='Особенности')
    dimensions = models.CharField(max_length=200, blank=True, null=True, verbose_name='Размеры')
    color = models.CharField(max_length=100, choices=COLOR_CHOICES, verbose_name='Цвет')

    class Meta:
        verbose_name = 'Держатель для телефона'
        verbose_name_plural = 'Держатели для телефонов'

    def __str__(self):
        return f"{self.id}-{self.product} - {self.type}"


class SmartphoneDisplaysCharacteristic(models.Model):
    product = models.ManyToManyField(Product, related_name='smartphone_displays', verbose_name='Продукт')
    type = models.CharField(max_length=200, verbose_name='Тип')
    diagonal = models.DecimalField(max_digits=5, decimal_places=2, verbose_name='Диагональ экрана')
    compatible_brand = models.ForeignKey('Brand', on_delete=models.CASCADE, verbose_name='Совместимый бренд')
    compatible_models = models.CharField(max_length=200, verbose_name='Совместимые модели')

    additional_information = models.CharField(max_length=150, blank=True, null=True,
                                              verbose_name='Дополнительная информация')
    equipment = models.CharField(max_length=200, verbose_name='Комплектация', blank=True, null=True)
    color = models.CharField(max_length=100, choices=COLOR_CHOICES,blank=True, null=True, verbose_name='Цвет')

    class Meta:
        verbose_name = 'Дисплеи для смартфонов'
        verbose_name_plural = 'Дисплеи для смартфонов'

    def __str__(self):
        return f"{self.id}-{self.product} - {self.type}"


class ChargingDeviceCharacteristic(models.Model):
    product = models.ManyToManyField(Product, related_name='charging_device', verbose_name='Продукт')
    type = models.CharField(max_length=200, verbose_name='Тип')
    number_connected_devices = models.SmallIntegerField(verbose_name='Количество подключаемых устройств')
    maximum_output_current = models.CharField(max_length=200, blank=True, null=True, verbose_name='Максимальный выходной ток')
    voltage = models.CharField(max_length=200, blank=True, null=True, verbose_name='Напряжение')
    length = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True, verbose_name='Длина устройства')
    fast_charging_support = models.CharField(max_length=100, choices=QUESTION, verbose_name='Поддержка быстрой зарядки')
    detachable_cable = models.CharField(max_length=100, choices=QUESTION, blank=True, null=True, verbose_name='Съемный кабель')
    connectors = models.CharField(max_length=200, verbose_name='Разъем подключения')
    peculiarities = models.CharField(max_length=200, blank=True, null=True, verbose_name='Особенности')
    compatible_models = models.CharField(max_length=200, blank=True, null=True, verbose_name='Совместимость')
    charging_power = models.DecimalField(max_digits=5, decimal_places=2, verbose_name='Мощность зарядки')
    fast_charging_standard = models.CharField(max_length=200, blank=True, null=True, verbose_name='Стандарт быстрой зарядки')
    additional_information = models.CharField(max_length=150, blank=True, null=True,
                                              verbose_name='Дополнительная информация')
    color = models.CharField(max_length=100, choices=COLOR_CHOICES,blank=True, null=True, verbose_name='Цвет')

    class Meta:
        verbose_name = 'Зарядные устройства'
        verbose_name_plural = 'Зарядное устройство'

    def __str__(self):
        return f"{self.id}-{self.product} - {self.type}"


class ProtectiveFilmsGlassesCharacteristic(models.Model):
    product = models.ManyToManyField(Product, related_name='protective_films_glasses', verbose_name='Продукт')
    type = models.CharField(max_length=200, verbose_name='Тип')
    compatible_brand = models.ForeignKey('Brand', on_delete=models.CASCADE, verbose_name='Совместимый бренд')
    compatible_models = models.CharField(max_length=200, verbose_name='Совместимые модели')
    purpose = models.CharField(max_length=200, verbose_name='Назначение')
    type_of_coverage = models.CharField(max_length=200, verbose_name='Вид покрытия')
    thickness = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True, verbose_name='Толщина')
    hardness = models.SmallIntegerField(blank=True, null=True, verbose_name='Твердость')
    equipment = models.CharField(max_length=200, blank=True, null=True, verbose_name='Комплектация')
    color = models.CharField(max_length=100, choices=COLOR_CHOICES, blank=True, null=True, verbose_name='Цвет')

    class Meta:
        verbose_name = 'Защитные пленки и стекла для смартфонов'
        verbose_name_plural = 'Защитные пленки и стекла для смартфонов'

    def __str__(self):
        return f"{self.id}-{self.product} - {self.type}"


class StickersPhonesCharacteristic(models.Model):
    product = models.ManyToManyField(Product, related_name='stickers_phones', verbose_name='Продукт')
    number_pieces_package = models.SmallIntegerField(verbose_name='Количество штук в упаковке')
    housing_material = models.CharField(max_length=200, verbose_name='Материал')
    dimensions = models.CharField(max_length=100, verbose_name='Размеры')
    additional_information = models.CharField(max_length=150, blank=True, null=True,
                                              verbose_name='Дополнительно')
    color = models.CharField(max_length=100, choices=COLOR_CHOICES, blank=True, null=True, verbose_name='Цвет')

    class Meta:
        verbose_name = 'Наклейки для телефонов'
        verbose_name_plural = 'Наклейка для телефона'

    def __str__(self):
        return f"{self.id}-{self.product}"


class SmartphoneCasesCharacteristic(models.Model):
    product = models.ManyToManyField(Product, related_name='smartphone_cases', verbose_name='Продукт')
    type = models.CharField(max_length=200, verbose_name='Тип')
    compatible_brand = models.ForeignKey('Brand', on_delete=models.CASCADE, verbose_name='Совместимый бренд')
    housing_material = models.CharField(max_length=200, verbose_name='Материал')
    peculiarities = models.CharField(max_length=200, verbose_name='Особенности')
    compatible_models = models.CharField(max_length=200, verbose_name='Совместимые модели')
    manufacturer_code = models.CharField(max_length=200, blank=True, null=True, verbose_name='Модель/Артикул производителя')
    color = models.CharField(max_length=100, choices=COLOR_CHOICES, blank=True, null=True, verbose_name='Цвет')

    class Meta:
        verbose_name = 'Чехлы для смартфонов'
        verbose_name_plural = 'Чехл для смартфона'

    def __str__(self):
        return f"{self.id}-{self.product} - {self.type}"


class ProtectiveFilmsGlassesSmartFitnessBraceletsCharacteristic(models.Model):
    product = models.ManyToManyField(Product, related_name='protective_films', verbose_name='Продукт')
    type = models.CharField(max_length=200, verbose_name='Тип')
    purpose = models.CharField(max_length=200, verbose_name='Назначение')
    compatible_brand = models.ForeignKey('Brand', on_delete=models.CASCADE, verbose_name='Совместимый бренд')
    diagonal = models.DecimalField(max_digits=5, decimal_places=2, verbose_name='Размер дисплея')
    compatible_models = models.CharField(max_length=200, verbose_name='Совместимые модели')
    type_of_coverage = models.CharField(max_length=200, verbose_name='Вид покрытия')
    additional_information = models.CharField(max_length=150, blank=True, null=True,
                                              verbose_name='Дополнительно')
    manufacturer_code = models.CharField(max_length=200, blank=True, null=True, verbose_name='Модель/Артикул производителя')
    color = models.CharField(max_length=100, choices=COLOR_CHOICES, blank=True, null=True, verbose_name='Цвет')

    class Meta:
        verbose_name = 'Защитные пленки и стекла для смарт-часов, фитнес-браслетов'
        verbose_name_plural = 'Защитные пленки и стекла для смарт-часов, фитнес-браслетов'

    def __str__(self):
        return f"{self.id}-{self.product} - {self.type}"


class StrapsSmartWatchesCharacteristic(models.Model):
    product = models.ManyToManyField(Product, related_name='straps_smart_watches', verbose_name='Продукт')
    housing_material = models.CharField(max_length=200, verbose_name='Материал')
    type = models.CharField(max_length=200, verbose_name='Тип')
    compatible_brand = models.ForeignKey('Brand', on_delete=models.CASCADE, verbose_name='Совместимый бренд')
    compatible_models = models.CharField(max_length=200, verbose_name='Совместимые модели')
    length = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True, verbose_name='Длина')
    mounting_width = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True, verbose_name='Ширина крепления')
    additional_information = models.CharField(max_length=150, blank=True, null=True,
                                              verbose_name='Дополнительно')
    dimensions = models.CharField(max_length=100, verbose_name='Размер корпуса')
    color = models.CharField(max_length=100, choices=COLOR_CHOICES, verbose_name='Цвет')

    class Meta:
        verbose_name = 'Ремешки для смарт-часов и фитнес-браслетов'
        verbose_name_plural = 'Ремешки для смарт-часов и фитнес-браслетов'

    def __str__(self):
        return f"{self.id}-{self.product} - {self.type}"


class SmartWatchCharacteristic(models.Model):
    product = models.ManyToManyField(Product, related_name='smart_watch', verbose_name='Продукт')
    audio_input_output = models.CharField(max_length=400, blank=True, null=True, verbose_name='Ввод/вывод звука')
    audio_video = models.CharField(max_length=400, blank=True, null=True, verbose_name='Аудио/видео')
    headphone_jack = models.CharField(max_length=400, blank=True, null=True, verbose_name='Разъем для наушников')
    availability_camera = models.CharField(max_length=100, choices=QUESTION, blank=True, null=True, verbose_name='Наличие фотокамеры')

    operating_system = models.CharField(max_length=100, choices=OPERATING_SYSTEM, blank=True, null=True, verbose_name='Операционная система')
    type = models.CharField(max_length=200, verbose_name='Тип')
    platform_support = models.CharField(max_length=100, choices=OPERATING_SYSTEM, verbose_name='Поддержка платформ')
    mobile_device_support = models.CharField(max_length=400, blank=True, null=True, verbose_name='Поддержка мобильных устройств')
    installing_applications = models.CharField(max_length=100, choices=QUESTION, blank=True, null=True, verbose_name='Установка сторонних приложений')
    notifications = models.CharField(max_length=400, blank=True, null=True, verbose_name='Уведомления с просмотром или ответом')
    vibration = models.CharField(max_length=100, choices=QUESTION, blank=True, null=True, verbose_name='Вибрация')

    housing_material = models.CharField(max_length=200, blank=True, null=True, verbose_name='Материал корпуса ')
    bracelet_strap_material = models.CharField(max_length=200, verbose_name='Материал браслета/ремешка')
    case_color = models.CharField(max_length=100, choices=COLOR_CHOICES, verbose_name='Цвет корпуса')
    time_display_method = models.CharField(max_length=200, blank=True, null=True, verbose_name='Способ отображения времени')
    bracelet_strap_color = models.CharField(max_length=100, choices=COLOR_CHOICES, verbose_name='Цвет браслета/ремешка')
    glass_type = models.CharField(max_length=200, blank=True, null=True, verbose_name='Тип стекла')
    bracelet_strap_features = models.CharField(max_length=400, blank=True, null=True, verbose_name='Особенности браслета/ремешка')
    protection = models.CharField(max_length=200, verbose_name='Защита')
    moisture_protection_class = models.CharField(max_length=200, verbose_name='Класс влагозащиты')
    dimensions = models.CharField(max_length=200, blank=True, null=True, verbose_name='Габариты (ШхВхТ)')
    weight = models.IntegerField(blank=True, null=True, verbose_name='Вес')
    color = models.CharField(max_length=100, choices=COLOR_CHOICES, verbose_name='Цвет')
    case_size = models.CharField(max_length=100, verbose_name='Размер корпуса')

    phone_calls = models.CharField(max_length=200, verbose_name='Телефонные звонки')
    mobile_internet = models.CharField(max_length=200, blank=True, null=True, verbose_name='Мобильный интернет')
    gps = models.CharField(max_length=100, choices=QUESTION, blank=True, null=True, verbose_name='GPS')
    interfaces = models.CharField(max_length=200, verbose_name='Интерфейсы')

    availability_screen = models.CharField(max_length=100, choices=QUESTION, blank=True, null=True, verbose_name='Наличие экрана')
    screen_technology = models.CharField(max_length=200, blank=True, null=True, verbose_name='Технология экрана')
    screen_functions = models.CharField(max_length=200, verbose_name='Функции экрана')
    diagonal = models.DecimalField(max_digits=5, decimal_places=2, verbose_name='Диагональ')

    cpu = models.CharField(max_length=200, blank=True, null=True, verbose_name='Процессор')
    memory_card_slot = models.CharField(max_length=100, choices=QUESTION, blank=True, null=True, verbose_name='Слот для карты памяти')
    built_in_memory_capacity = models.IntegerField(blank=True, null=True, verbose_name='Объем встроенной памяти')

    monitoring = models.CharField(max_length=200, verbose_name='Мониторинг')
    sensors = models.CharField(max_length=500, verbose_name='Датчики')
    additional_information = models.CharField(max_length=150, blank=True, null=True,
                                              verbose_name='Дополнительная информация')

    battery = models.CharField(max_length=200, verbose_name='Аккумулятор')
    charging_connector_type = models.CharField(max_length=200, blank=True, null=True, verbose_name='Тип разъема для зарядки')
    charging_features = models.CharField(max_length=200, blank=True, null=True, verbose_name='Особенности зарядки')
    working_hours = models.CharField(max_length=200, verbose_name='Время работы')

    class Meta:
        verbose_name = 'Смарт-часы'
        verbose_name_plural = 'Смарт-часы'

    def __str__(self):
        return f"{self.id}-{self.product} - {self.type}"


class FitnessBraceletsCharacteristic(models.Model):
    product = models.ManyToManyField(Product, related_name='fitness_bracelets', verbose_name='Продукт')
    headphone_jack = models.CharField(max_length=400, verbose_name='Разъем для наушников')
    availability_camera = models.CharField(max_length=100, choices=QUESTION, blank=True, null=True, verbose_name='Наличие фотокамеры')
    type = models.CharField(max_length=200, verbose_name='Тип')
    platform_support = models.CharField(max_length=100, choices=OPERATING_SYSTEM, verbose_name='Поддержка платформ')
    mobile_device_support = models.CharField(max_length=400, blank=True, null=True, verbose_name='Поддержка мобильных устройств')
    installing_applications = models.CharField(max_length=100, choices=QUESTION, blank=True, null=True, verbose_name='Установка сторонних приложений')
    notifications = models.CharField(max_length=400, blank=True, null=True, verbose_name='Уведомления с просмотром или ответом')
    vibration = models.CharField(max_length=100, choices=QUESTION, blank=True, null=True, verbose_name='Вибрация')

    housing_material = models.CharField(max_length=200, blank=True, null=True, verbose_name='Материал корпуса ')
    bracelet_strap_material = models.CharField(max_length=200, verbose_name='Материал браслета/ремешка')
    case_color = models.CharField(max_length=100, choices=COLOR_CHOICES, verbose_name='Цвет корпуса')
    time_display_method = models.CharField(max_length=200, verbose_name='Способ отображения времени')
    bracelet_strap_color = models.CharField(max_length=100, choices=COLOR_CHOICES, verbose_name='Цвет браслета/ремешка')
    glass_type = models.CharField(max_length=200, verbose_name='Тип стекла')
    bracelet_strap_features = models.CharField(max_length=400, verbose_name='Особенности браслета/ремешка')
    protection = models.CharField(max_length=200, verbose_name='Защита')
    moisture_protection_class = models.CharField(max_length=200, verbose_name='Класс влагозащиты')
    dimensions = models.CharField(max_length=200, verbose_name='Габариты (ШхВхТ)')
    weight = models.IntegerField(verbose_name='Вес')
    color = models.CharField(max_length=100, choices=COLOR_CHOICES, verbose_name='Цвет')
    case_size = models.CharField(max_length=100, blank=True, null=True, verbose_name='Размер корпуса')

    phone_calls = models.CharField(max_length=200, verbose_name='Телефонные звонки')
    mobile_internet = models.CharField(max_length=200, verbose_name='Мобильный интернет')
    gps = models.CharField(max_length=100, choices=QUESTION, verbose_name='GPS')
    interfaces = models.CharField(max_length=200, verbose_name='Интерфейсы')

    availability_screen = models.CharField(max_length=100, choices=QUESTION, verbose_name='Наличие экрана')
    screen_technology = models.CharField(max_length=200, verbose_name='Технология экрана')
    screen_functions = models.CharField(max_length=200, verbose_name='Функции экрана')
    diagonal = models.DecimalField(max_digits=5, decimal_places=2, verbose_name='Диагональ')

    memory_card_slot = models.CharField(max_length=100, choices=QUESTION, verbose_name='Слот для карты памяти')

    monitoring = models.CharField(max_length=200, verbose_name='Мониторинг')
    sensors = models.CharField(max_length=500, verbose_name='Датчики')
    additional_information = models.CharField(max_length=150, blank=True, null=True,
                                              verbose_name='Дополнительная информация')

    battery = models.CharField(max_length=200, verbose_name='Аккумулятор')
    charging_connector_type = models.CharField(max_length=200, verbose_name='Тип разъема для зарядки')
    charging_features = models.CharField(max_length=200, blank=True, null=True, verbose_name='Особенности зарядки')
    working_hours = models.CharField(max_length=200, verbose_name='Время работы')

    class Meta:
        verbose_name = 'Фитнес-браслеты'
        verbose_name_plural = 'Фитнес-браслеты'

    def __str__(self):
        return f"{self.id}-{self.product} - {self.type}"


class CFSWFBCharacteristic(models.Model):
    product = models.ManyToManyField(Product, related_name='cases_smart_watches', verbose_name='Продукт')
    dial_size = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True, verbose_name='Размер циферблата')
    type = models.CharField(max_length=200, verbose_name='Тип')
    housing_material = models.CharField(max_length=200, verbose_name='Материал')
    case_color = models.CharField(max_length=100, choices=COLOR_CHOICES, verbose_name='Цвет корпуса')
    compatible_brand = models.ForeignKey('Brand', on_delete=models.CASCADE, verbose_name='Совместимый бренд')
    color = models.CharField(max_length=100, choices=COLOR_CHOICES, blank=True, null=True, verbose_name='Цвет браслета/ремешка')
    additional_information = models.CharField(max_length=500, blank=True, null=True,
                                              verbose_name='Дополнительно')

    class Meta:
        verbose_name = 'Чехлы для смарт-часов и фитнес-браслетов'
        verbose_name_plural = 'Чехлы для смарт-часов и фитнес-браслетов'

    def __str__(self):
        return f"{self.id}-{self.product} - {self.type}"


class CellPhonesCharacteristic(models.Model):
    product = models.ManyToManyField(Product, related_name='cell_phones', verbose_name='Продукт')
    type = models.CharField(max_length=200, verbose_name='Тип')
    standard = models.CharField(max_length=200, verbose_name='Стандарт')
    sim_card_type = models.CharField(max_length=200, verbose_name='Тип SIM-карты')
    number_sim_cards = models.IntegerField(verbose_name='Количество SIM-карт')
    multi_sim_mode = models.CharField(max_length=200, verbose_name='Режим работы нескольких SIM-карт')
    housing_material = models.CharField(max_length=200, verbose_name='Материал')
    screen_type = models.CharField(max_length=200, verbose_name='Тип экрана')
    diagonal = models.DecimalField(max_digits=5, decimal_places=2, verbose_name='Диагональ')
    screen_resolution = models.CharField(max_length=200, blank=True, null=True, verbose_name='Разрешение экрана')
    scratch_resistant_glass = models.CharField(max_length=100, blank=True, null=True, choices=QUESTION, verbose_name='Устойчивое к царапинам стекло')
    cpu = models.CharField(max_length=200, blank=True, null=True, verbose_name='Объем оперативной памяти')
    color = models.CharField(max_length=100, choices=COLOR_CHOICES, blank=True, null=True, verbose_name='Цвет')

    built_in_memory_capacity = models.IntegerField(blank=True, null=True, verbose_name='Объем встроенной памяти')
    memory_card_slot = models.CharField(max_length=100, choices=QUESTION, verbose_name='Слот карт памяти')
    interfaces = models.CharField(max_length=200, blank=True, null=True, verbose_name='Интерфейсы')
    access_internet = models.CharField(max_length=100, choices=QUESTION, blank=True, null=True, verbose_name='Доступ в интернет')
    camera = models.CharField(max_length=100, choices=QUESTION, blank=True, null=True, verbose_name='Фотокамера')
    audio_formats = models.CharField(max_length=200, verbose_name='Форматы аудио')
    battery_type = models.CharField(max_length=200, verbose_name='Тип аккумулятора')
    battery_capacity = models.DecimalField(max_digits=7, decimal_places=2, verbose_name='Емкость аккумулятора')
    talk_time = models.DecimalField(max_digits=7, decimal_places=2, verbose_name='Время разговора')
    waiting_time = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True, verbose_name='Время ожидания')
    charging_time = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True, verbose_name='Время заряда')
    operating_time_listening_music = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True, verbose_name='Время работы в режиме прослушивания музыки')
    dimensions = models.CharField(max_length=200, blank=True, null=True, verbose_name='Габариты')
    weight = models.IntegerField(blank=True, null=True, verbose_name='Вес')
    peculiarities = models.CharField(max_length=500, verbose_name='Особенности', blank=True, null=True)
    equipment = models.CharField(max_length=200, verbose_name='Комплектация', blank=True, null=True)

    class Meta:
        verbose_name = 'Мобильный телефон'
        verbose_name_plural = 'Мобильные телефоны'

    def __str__(self):
        return f"{self.id}-{self.product} - {self.type}"


class SatellitePhonesCharacteristic(models.Model):
    product = models.ManyToManyField(Product, related_name='satellite_phones', verbose_name='Продукт')
    housing_material = models.CharField(max_length=200, blank=True, null=True, verbose_name='Материал корпуса ')
    type = models.CharField(max_length=200, verbose_name='Тип')
    operating_system = models.CharField(max_length=100, choices=OPERATING_SYSTEM, verbose_name='Операционная система')
    number_sim_cards = models.IntegerField(verbose_name='Количество SIM-карт')
    sim_card_type = models.CharField(max_length=200, verbose_name='Тип SIM-карты')
    multi_sim_mode = models.CharField(max_length=200, blank=True, null=True, verbose_name='Режим работы нескольких SIM-карт')
    number_of_antennas = models.IntegerField(verbose_name='Количество антенн')
    transmission_speed = models.DecimalField(max_digits=5, decimal_places=2, verbose_name='Скорость передачи')
    frequency_range_hz = models.CharField(max_length=200, blank=True, null=True, verbose_name='Частотный диапазон, Гц')
    gsm = models.CharField(max_length=200, blank=True, null=True, verbose_name='GSM')
    navigation = models.CharField(max_length=200, blank=True, null=True, verbose_name='Навигация')
    ram = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True, verbose_name='Объем оперативной памяти')
    hdd_memory = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True, verbose_name='Объем встроенной памяти')
    interfaces = models.CharField(max_length=200, blank=True, null=True, verbose_name='Интерфейсы')
    sos_button = models.CharField(max_length=100, choices=QUESTION, verbose_name='Кнопка SOS')
    touch_screen = models.CharField(max_length=100, choices=QUESTION, blank=True, null=True, verbose_name='Сенсорный экран')
    screen_type = models.CharField(max_length=200, blank=True, null=True, verbose_name='Тип экрана')
    diagonal = models.DecimalField(max_digits=5, blank=True, null=True, decimal_places=2, verbose_name='Диагональ')
    color = models.CharField(max_length=100, choices=COLOR_CHOICES, verbose_name='Цвет')

    battery_capacity = models.DecimalField(max_digits=7, decimal_places=2, verbose_name='Емкость аккумулятора')
    battery_type = models.CharField(max_length=200, verbose_name='Тип аккумулятора')
    talk_time = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True, verbose_name='Время работы в режиме разговора')
    waiting_time = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True,
                                       verbose_name='Время работы в режиме ожидания')
    protection_class = models.CharField(max_length=200, verbose_name='Класс защиты')
    operating_temperature = models.CharField(max_length=200, blank=True, null=True, verbose_name='Рабочая температура, °С')
    operating_humidity = models.CharField(max_length=200, blank=True, null=True, verbose_name='Рабочая влажность,%')

    width = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True, verbose_name='Ширина')
    height = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True, verbose_name='Высота')
    length = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True, verbose_name='Длина')
    weight = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True, verbose_name='Вес')

    class Meta:
        verbose_name = 'Спутниковые телефоны и коммуникаторы'
        verbose_name_plural = 'Спутниковые телефоны и коммуникаторы'

    def __str__(self):
        return f"{self.id}-{self.product} - {self.type}"


class RAMCharacteristic(models.Model):
    product = models.ManyToManyField(Product, related_name='rams', verbose_name='Продукт')
    product_name_copy = models.CharField(max_length=500, blank=True, null=True, verbose_name='Наименование товара')

    memory_type = models.CharField(max_length=100, choices=MEMORY_TYPE, verbose_name='Тип памяти')
    form_factor = models.CharField(max_length=200, verbose_name='Форм-фактор')
    volume_one_module = models.DecimalField(max_digits=12, decimal_places=2, verbose_name='Объем одного модуля')
    number_modules_included = models.IntegerField(verbose_name='Количество модулей в комплекте')
    clock_frequency = models.DecimalField(max_digits=12, decimal_places=2, verbose_name='Тактовая частота')
    bandwidth = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True, verbose_name='Пропускная способность')
    ecc_support = models.CharField(max_length=100, choices=QUESTION, blank=True, null=True, verbose_name='Поддержка ECC')
    number_contacts = models.IntegerField(blank=True, null=True, verbose_name='Количество контактов')
    buffered = models.CharField(max_length=100, choices=QUESTION, blank=True, null=True, verbose_name='Буферизованная (registered)')
    low_profile = models.CharField(max_length=100, choices=QUESTION, blank=True, null=True, verbose_name='Низкопрофильная (low profile)')
    total_memory_capacity = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True, verbose_name='Суммарный объем памяти')

    timings = models.CharField(max_length=400, blank=True, null=True, verbose_name='Тайминги')
    supply_voltage = models.DecimalField(max_digits=12, decimal_places=2, verbose_name='Напряжение питания')
    radiator = models.CharField(max_length=100, choices=QUESTION, verbose_name='Радиатор')

    color = models.CharField(max_length=100, choices=COLOR_CHOICES, verbose_name='Цвет')

    class Meta:
        verbose_name = 'Оперативная память'
        verbose_name_plural = 'Оперативная память'

    def __str__(self):
        return f"{self.id}-{self.product} - {self.memory_type}"


class DiskDrivesCharacteristic(models.Model):
    product = models.ManyToManyField(Product, related_name='disk_drives', verbose_name='Продукт')
    product_name_copy = models.CharField(max_length=500, blank=True, null=True, verbose_name='Наименование товара')
    type = models.CharField(max_length=100, choices=DISK_TYPE, verbose_name='Тип')
    form_factor = models.CharField(max_length=200, verbose_name='Форм-фактор')
    flash_memory_type = models.CharField(max_length=200, blank=True, null=True, verbose_name='Тип флэш-памяти')
    battery_capacity = models.DecimalField(max_digits=7, decimal_places=2, verbose_name='Емкость')
    interfaces = models.CharField(max_length=200, verbose_name='Интерфейсы')
    reading_speed = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True, verbose_name='Скорость чтения')
    write_speed = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True, verbose_name='Скорость записи')
    mtbf = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True, verbose_name='Время наработки на отказ')
    noise_level = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True, verbose_name='Уровень шума')
    dimensions = models.CharField(max_length=200, blank=True, null=True, verbose_name='Размеры')
    weight = models.IntegerField(blank=True, null=True, verbose_name='Вес')
    srs = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True, verbose_name='Ударостойкость при хранении')
    ir = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True, verbose_name='Ударостойкость при работе')

    color = models.CharField(max_length=100, choices=COLOR_CHOICES, blank=True, null=True, verbose_name='Цвет браслета/ремешка')

    class Meta:
        verbose_name = 'Жесткие диски и твердотельные накопители'
        verbose_name_plural = 'Жесткие диски и твердотельные накопители'

    def __str__(self):
        return f"{self.id}-{self.product} - {self.type}"


class VideoCardsCharacteristic(models.Model):
    product = models.ManyToManyField(Product, related_name='video_cards', verbose_name='Продукт')
    product_name_copy = models.CharField(max_length=500, blank=True, null=True, verbose_name='Наименование товара')
    series = models.CharField(max_length=300, verbose_name='Серия')
    interfaces = models.CharField(max_length=200, verbose_name='Интерфейсы')
    technical_process = models.DecimalField(max_digits=12, blank=True, null=True, decimal_places=2, verbose_name='Техпроцесс')
    max_monitors_supported = models.IntegerField(verbose_name='Количество поддерживаемых мониторов')
    gpu_manufacturer = models.CharField(max_length=200, blank=True, null=True, verbose_name='Производитель графического процессора')
    max_resolution = models.CharField(max_length=200, blank=True, null=True, verbose_name='Максимальное разрешение')

    gpu_frequency = models.IntegerField(blank=True, null=True, verbose_name='Частота графического процессора')
    vm_capacity = models.DecimalField(max_digits=12, decimal_places=2, verbose_name='Объем видеопамяти')
    vm_type = models.CharField(max_length=100, choices=VIDEO_MEMORY_TYPE, verbose_name='Тип видеопамяти')
    vm_frequency = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True, verbose_name='Частота видеопамяти')
    vm_bus_width = models.DecimalField(max_digits=12, decimal_places=2, verbose_name='Разрядность шины видеопамяти')
    mode_support = models.CharField(max_length=100, choices=QUESTION, blank=True, null=True, verbose_name='Поддержка режима SLI/CrossFire')
    number_universal_processors = models.IntegerField(blank=True, null=True, verbose_name='Число универсальных процессоров')

    lrh = models.CharField(max_length=100, choices=QUESTION, blank=True, null=True, verbose_name='LHR (Lite Hash Rate)')
    vc_length = models.CharField(max_length=200, blank=True, null=True, verbose_name='Длина видеокарты')
    connectors = models.CharField(max_length=200, blank=True, null=True, verbose_name='Разъемы')
    recommended_power_supply = models.DecimalField(max_digits=12, decimal_places=2, verbose_name='Рекомендуемая мощность блока питания')
    power_connectors = models.CharField(max_length=200, blank=True, null=True, verbose_name='Разъемы питания')
    number_occupied_slots = models.IntegerField(blank=True, null=True, verbose_name='Количество занимаемых слотов')
    number_fans = models.IntegerField(blank=True, null=True, verbose_name='Количество вентиляторов')
    cooling_type = models.CharField(max_length=200, blank=True, null=True, verbose_name='Тип охлаждения')

    color = models.CharField(max_length=100, choices=COLOR_CHOICES, blank=True, null=True, verbose_name='Цвет')

    class Meta:
        verbose_name = 'Видеокарты'
        verbose_name_plural = 'Видеокарты'

    def __str__(self):
        return f"{self.id}-{self.product} - {self.product_name_copy}"


class ProcessorsCharacteristic(models.Model):
    product = models.ManyToManyField(Product, related_name='processors', verbose_name='Продукт')
    product_name_copy = models.CharField(max_length=500, blank=True, null=True, verbose_name='Наименование товара')
    power_dissipation = models.DecimalField(max_digits=12, decimal_places=2, verbose_name='Рассеиваемая мощность')
    critical_temperature = models.DecimalField(max_digits=12, decimal_places=2, verbose_name='Критическая температура')

    processor_type = models.CharField(max_length=300, verbose_name='Тип процессора')
    socket = models.CharField(max_length=300, verbose_name='Сокет')
    base_cpu_frequency = models.DecimalField(max_digits=12, decimal_places=2, verbose_name='Базовая частота процессора')
    multiplication_factor = models.IntegerField(blank=True, null=True, verbose_name='Коэффициент умножения')
    integrated_graphics_core = models.CharField(max_length=100, choices=QUESTION, verbose_name='Интегрированное графическое ядро')
    graphics_core = models.CharField(max_length=300, blank=True, null=True, verbose_name='Графическое ядро')
    supported_technologies = models.CharField(max_length=300, blank=True, null=True, verbose_name='Поддерживаемые технологии')
    supported_instructions = models.CharField(max_length=300, blank=True, null=True, verbose_name='Поддерживаемые инструкции')
    max_processor_frequency = models.DecimalField(max_digits=12, decimal_places=2, verbose_name='Максимальная частота процессора')
    number_threads = models.IntegerField(blank=True, null=True, verbose_name='Количество потоков')
    memory = models.CharField(max_length=300, blank=True, null=True, verbose_name='Память')
    delivery_type = models.CharField(max_length=300, verbose_name='Тип поставки')

    kernel_type = models.CharField(max_length=300, blank=True, null=True, verbose_name='Тип ядра')
    number_cores = models.IntegerField(verbose_name='Количество ядер')
    core = models.CharField(max_length=300, blank=True, null=True, verbose_name='Ядро')
    technical_process = models.DecimalField(max_digits=12, decimal_places=2, verbose_name='Техпроцесс')
    l1 = models.DecimalField(max_digits=12, blank=True, null=True, decimal_places=2, verbose_name='Объем кэша L1')
    l2 = models.DecimalField(max_digits=12, blank=True, null=True, decimal_places=2, verbose_name='Объем кэша L2')
    l3 = models.DecimalField(max_digits=12, blank=True, null=True, decimal_places=2, verbose_name='Объем кэша L3')

    color = models.CharField(max_length=100, choices=COLOR_CHOICES, blank=True, null=True, verbose_name='Цвет')

    class Meta:
        verbose_name = 'Видеокарты'
        verbose_name_plural = 'Видеокарты'

    def __str__(self):
        return f"{self.id}-{self.product} - {self.product_name_copy}"


class SystemUnitsCharacteristic(models.Model):
    product = models.ManyToManyField(Product, related_name='system_units', verbose_name='Продукт')
    product_name_copy = models.CharField(max_length=500, blank=True, null=True, verbose_name='Наименование товара')

    operating_system = models.CharField(max_length=100, choices=OPERATING_SYSTEM, verbose_name='Установленная ОС')
    power_supply_power = models.DecimalField(max_digits=12, blank=True, null=True, decimal_places=2, verbose_name='Мощность блока питания')
    game = models.CharField(max_length=100, choices=QUESTION, verbose_name='Игровой')

    cpu = models.CharField(max_length=400, verbose_name='Процессор')
    number_processor_cores = models.IntegerField(verbose_name='Количество ядер процессора')
    ram_type = models.CharField(max_length=100, choices=MEMORY_TYPE, verbose_name='Тип оперативной памяти')
    base_cpu_frequency = models.DecimalField(max_digits=12, blank=True, null=True, decimal_places=2, verbose_name='Базовая частота процессора')
    max_processor_frequency = models.DecimalField(max_digits=12, blank=True, null=True, decimal_places=2, verbose_name='Максимальная частота процессора')
    ram_size = models.DecimalField(max_digits=12, decimal_places=2, verbose_name='Размер оперативной памяти')
    ram_frequency = models.DecimalField(max_digits=12, decimal_places=2, verbose_name='Частота оперативной памяти')
    vc_type = models.CharField(max_length=400, verbose_name='Тип видеокарты')
    ssd_capacity = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True, verbose_name='Объем SSD')
    hdd_capacity = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True, verbose_name='Объем HDD')
    video_processor = models.CharField(max_length=400, verbose_name='Видеопроцессор')
    drive_type = models.CharField(max_length=100, choices=DISK_TYPE, verbose_name='Тип накопителя')
    video_memory_size = models.DecimalField(max_digits=12, decimal_places=2, verbose_name='Размер видеопамяти')
    number_installed_modules = models.IntegerField(blank=True, null=True, verbose_name='Количество установленных модулей')

    optical_drive_type = models.CharField(max_length=100, choices=QUESTION, blank=True, null=True, verbose_name='Тип оптического привода')
    wifi = models.CharField(max_length=100, choices=QUESTION, blank=True, null=True, verbose_name='Wi-Fi')
    card_reader = models.CharField(max_length=100, choices=QUESTION, blank=True, null=True, verbose_name='Картридер')

    case_form_factor = models.CharField(max_length=400, verbose_name='Форм-фактор корпуса')
    dimensions = models.CharField(max_length=200, blank=True, null=True, verbose_name='Габариты (ДхШхТ)')
    weight = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True, verbose_name='Вес')
    equipment = models.CharField(max_length=200, blank=True, null=True, verbose_name='Комплектация')
    color = models.CharField(max_length=100, choices=COLOR_CHOICES,verbose_name='Цвет')

    class Meta:
        verbose_name = 'Системные блоки'
        verbose_name_plural = 'Системные блоки'

    def __str__(self):
        return f"{self.id}-{self.product} - {self.product_name_copy}"


class MonoblocksCharacteristic(models.Model):
    product = models.ManyToManyField(Product, related_name='monoblocks', verbose_name='Продукт')
    product_name_copy = models.CharField(max_length=500, blank=True, null=True, verbose_name='Наименование товара')

    operating_system = models.CharField(max_length=100, choices=OPERATING_SYSTEM, verbose_name='Операционная система')

    scr_diagonal = models.DecimalField(max_digits=12, decimal_places=2, verbose_name='Диагональ экрана')
    scr_resolution = models.CharField(max_length=500, verbose_name='Разрешение экрана')
    scr_matrix_type = models.CharField(max_length=500, verbose_name='Тип матрицы экрана')
    scr_covering_type = models.CharField(max_length=500, blank=True, null=True, verbose_name='Тип покрытия экрана')
    touch_scr = models.CharField(max_length=100, choices=QUESTION, verbose_name='Сенсорный экран')
    scr_brightness = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True, verbose_name='Яркость экрана')

    cpu = models.CharField(max_length=400, verbose_name='Процессор')
    number_processor_cores = models.IntegerField(verbose_name='Количество ядер процессора')
    base_cpu_frequency = models.DecimalField(max_digits=12, decimal_places=2, verbose_name='Базовая частота процессора')
    max_processor_frequency = models.DecimalField(max_digits=12, blank=True, null=True, decimal_places=2, verbose_name='Максимальная частота процессора')

    ram_size = models.DecimalField(max_digits=12, decimal_places=2, verbose_name='Размер оперативной памяти')
    ram_type = models.CharField(max_length=100, choices=MEMORY_TYPE, verbose_name='Тип оперативной памяти')
    ram_frequency = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True, verbose_name='Частота оперативной памяти')

    total_storage_capacity = models.DecimalField(max_digits=12, decimal_places=2, verbose_name='Общий объем накопителей')
    drive_type = models.CharField(max_length=100, choices=DISK_TYPE, verbose_name='Тип накопителя')
    ssd_capacity = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True, verbose_name='Объем SSD')
    hdd_capacity = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True, verbose_name='Объем HDD')

    video_processor = models.CharField(max_length=400, verbose_name='Видеокарта')
    vc_type = models.CharField(max_length=400, verbose_name='Тип видеокарты')
    video_memory_size = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True, verbose_name='Размер видеопамяти')

    video_resolution = models.CharField(max_length=400, blank=True, null=True, verbose_name='Разрешение видео')
    built_in_webcam = models.CharField(max_length=100, choices=QUESTION, verbose_name='Встроенная веб-камера')

    number_speakers = models.IntegerField(blank=True, null=True, verbose_name='Количество динамиков')
    built_in_speakers = models.CharField(max_length=100, choices=QUESTION, blank=True, null=True, verbose_name='Встроенные динамики')
    built_in_microphone = models.CharField(max_length=100, choices=QUESTION, blank=True, null=True, verbose_name='Встроенный микрофон')

    wifi = models.CharField(max_length=100, choices=QUESTION, verbose_name='Поддержка Wi-Fi')
    wifi_standard = models.CharField(max_length=400, blank=True, null=True, verbose_name='Стандарт Wi-Fi')
    bluetooth = models.CharField(max_length=100, choices=QUESTION, verbose_name='Поддержка Bluetooth')
    bluetooth_version = models.CharField(max_length=400, blank=True, null=True, verbose_name='Версия Bluetooth')
    optical_drive_type = models.CharField(max_length=100, choices=QUESTION, blank=True, null=True, verbose_name='Тип оптического привода')
    card_reader = models.CharField(max_length=100, choices=QUESTION, blank=True, null=True, verbose_name='Картридер')

    interfaces = models.CharField(max_length=200, blank=True, null=True, verbose_name='Интерфейсы')
    power_supply_power = models.DecimalField(max_digits=12, blank=True, null=True, decimal_places=2, verbose_name='Мощность блока питания')

    housing_material = models.CharField(max_length=200, blank=True, null=True, verbose_name='Материал корпуса')
    color = models.CharField(max_length=100, choices=COLOR_CHOICES,verbose_name='Цвет')

    equipment = models.CharField(max_length=200, blank=True, null=True, verbose_name='Комплектация')

    width = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True, verbose_name='Ширина')
    height = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True, verbose_name='Высота')
    thickness = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True, verbose_name='Толщина')
    weight = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True, verbose_name='Вес')

    class Meta:
        verbose_name = 'Моноблоки'
        verbose_name_plural = 'Моноблоки'

    def __str__(self):
        return f"{self.id}-{self.product} - {self.product_name_copy}"

