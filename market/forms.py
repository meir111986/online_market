from django import forms
from market.models import Product, BatteryCharacteristic, CableCharacteristic, ExternalBatteryCharacteristic, \
    PhoneHoldersCharacteristic, SmartphoneDisplaysCharacteristic, ChargingDeviceCharacteristic, \
    ProtectiveFilmsGlassesCharacteristic, StickersPhonesCharacteristic, SmartphoneCasesCharacteristic, \
    ProtectiveFilmsGlassesSmartFitnessBraceletsCharacteristic, StrapsSmartWatchesCharacteristic, \
    SmartWatchCharacteristic, FitnessBraceletsCharacteristic, CFSWFBCharacteristic, CellPhonesCharacteristic, \
    SatellitePhonesCharacteristic, RAMCharacteristic, DiskDrivesCharacteristic, VideoCardsCharacteristic, \
    ProcessorsCharacteristic, SystemUnitsCharacteristic, MonoblocksCharacteristic


class MultipleFileInput(forms.ClearableFileInput):
    allow_multiple_selected = True


class MultipleImageField(forms.ImageField):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault("widget", MultipleFileInput())
        kwargs.setdefault('required', False)
        super().__init__(*args, **kwargs)

    def clean(self, data, initial=None):
        single_file_clean = super().clean
        if isinstance(data, (list, tuple)):
            result = [single_file_clean(d, initial) for d in data]
        else:
            result = single_file_clean(data, initial)
        return result


class ProductForm(forms.ModelForm):
    image = MultipleImageField(required=True, label='Изображения')
    description = forms.CharField(widget=forms.Textarea(attrs={'cols': 45, 'rows': 3, "class": "form-control"}),
                                  label='Описание:', required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
            self.fields['brand'].widget.attrs['class'] = 'form-select'

    class Meta:
        model = Product
        fields = ['product_name', 'price', 'article', 'link_to_youtube', 'brand', 'image', 'description']


class ProductUpdateForm(forms.ModelForm):
    image = MultipleImageField(label='Изображения')
    description = forms.CharField(widget=forms.Textarea(attrs={'rows': 3, "class": "form-control"}),
                                  label='Описание:', required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
            self.fields['brand'].widget.attrs['class'] = 'form-select'

    class Meta:
        model = Product
        fields = ['product_name', 'price', 'article', 'link_to_youtube', 'brand', 'image', 'description']


class BatteryCharacteristicForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
            self.fields['color'].widget.attrs['class'] = 'form-select'

    class Meta:
        model = BatteryCharacteristic
        fields = ['type', 'battery_capacity', 'voltage', 'additional_information', 'dimensions', 'weight', 'color']


class CableCharacteristicForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
            self.fields['color'].widget.attrs['class'] = 'form-select'

    class Meta:
        model = CableCharacteristic
        fields = ['type', 'connectors', 'length', 'color']


class ExternalBatteryCharacteristicForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
            self.fields['color'].widget.attrs['class'] = 'form-select'
            self.fields['fast_charging_support'].widget.attrs['class'] = 'form-select'
            self.fields['solar_battery'].widget.attrs['class'] = 'form-select'
            self.fields['short_circuit_protection'].widget.attrs['class'] = 'form-select'
            self.fields['overload_protection'].widget.attrs['class'] = 'form-select'
            self.fields['overheat_protection'].widget.attrs['class'] = 'form-select'
            self.fields['wireless_charging_support'].widget.attrs['class'] = 'form-select'

    class Meta:
        model = ExternalBatteryCharacteristic
        fields = ['voltage', 'capacity', 'maximum_output_current', 'exits', 'connecting_power_source',
                  'number_of_USB_ports', 'purpose', 'built_in_battery_type', 'charger', 'fast_charging_support',
                  'solar_battery', 'short_circuit_protection', 'overload_protection', 'overheat_protection',
                  'wireless_charging_support', 'peculiarities', 'housing_material', 'equipment', 'dimensions', 'weight',
                  'color', ]


class PhoneHoldersCharacteristicForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
            self.fields['color'].widget.attrs['class'] = 'form-select'

    class Meta:
        model = PhoneHoldersCharacteristic
        exclude = ['product', ]


class SmartphoneDisplaysCharacteristicForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
            self.fields['compatible_brand'].widget.attrs['class'] = 'form-select'

    class Meta:
        model = SmartphoneDisplaysCharacteristic
        exclude = ['product', ]


class ChargingDeviceCharacteristicForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
            self.fields['fast_charging_support'].widget.attrs['class'] = 'form-select'
            self.fields['detachable_cable'].widget.attrs['class'] = 'form-select'
            self.fields['color'].widget.attrs['class'] = 'form-select'

    class Meta:
        model = ChargingDeviceCharacteristic
        exclude = ['product', ]


class ProtectiveFilmsGlassesCharacteristicForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
            self.fields['color'].widget.attrs['class'] = 'form-select'

    class Meta:
        model = ProtectiveFilmsGlassesCharacteristic
        exclude = ['product', ]


class StickersPhonesCharacteristicForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
            self.fields['color'].widget.attrs['class'] = 'form-select'

    class Meta:
        model = StickersPhonesCharacteristic
        exclude = ['product', ]


class SmartphoneCasesCharacteristicForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
            self.fields['color'].widget.attrs['class'] = 'form-select'
            self.fields['compatible_brand'].widget.attrs['class'] = 'form-select'

    class Meta:
        model = SmartphoneCasesCharacteristic
        exclude = ['product', ]


class ProtectiveFilmsGlassesSmartFitnessBraceletsCharacteristicForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
            self.fields['color'].widget.attrs['class'] = 'form-select'
            self.fields['compatible_brand'].widget.attrs['class'] = 'form-select'

    class Meta:
        model = ProtectiveFilmsGlassesSmartFitnessBraceletsCharacteristic
        exclude = ['product', ]


class StrapsSmartWatchesCharacteristicForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
            self.fields['color'].widget.attrs['class'] = 'form-select'
            self.fields['compatible_brand'].widget.attrs['class'] = 'form-select'

    class Meta:
        model = StrapsSmartWatchesCharacteristic
        exclude = ['product', ]


class SmartWatchCharacteristicForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
            self.fields['color'].widget.attrs['class'] = 'form-select'
            self.fields['availability_camera'].widget.attrs['class'] = 'form-select'
            self.fields['operating_system'].widget.attrs['class'] = 'form-select'
            self.fields['platform_support'].widget.attrs['class'] = 'form-select'
            self.fields['installing_applications'].widget.attrs['class'] = 'form-select'
            self.fields['vibration'].widget.attrs['class'] = 'form-select'
            self.fields['case_color'].widget.attrs['class'] = 'form-select'
            self.fields['bracelet_strap_color'].widget.attrs['class'] = 'form-select'
            self.fields['gps'].widget.attrs['class'] = 'form-select'
            self.fields['availability_screen'].widget.attrs['class'] = 'form-select'
            self.fields['memory_card_slot'].widget.attrs['class'] = 'form-select'

    class Meta:
        model = SmartWatchCharacteristic
        exclude = ['product', ]


class FitnessBraceletsCharacteristicForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
            self.fields['color'].widget.attrs['class'] = 'form-select'
            self.fields['availability_camera'].widget.attrs['class'] = 'form-select'
            self.fields['platform_support'].widget.attrs['class'] = 'form-select'
            self.fields['installing_applications'].widget.attrs['class'] = 'form-select'
            self.fields['vibration'].widget.attrs['class'] = 'form-select'
            self.fields['case_color'].widget.attrs['class'] = 'form-select'
            self.fields['bracelet_strap_color'].widget.attrs['class'] = 'form-select'
            self.fields['gps'].widget.attrs['class'] = 'form-select'
            self.fields['availability_screen'].widget.attrs['class'] = 'form-select'
            self.fields['memory_card_slot'].widget.attrs['class'] = 'form-select'

    class Meta:
        model = FitnessBraceletsCharacteristic
        exclude = ['product', ]


class CFSWFBCharacteristicForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
            self.fields['color'].widget.attrs['class'] = 'form-select'
            self.fields['case_color'].widget.attrs['class'] = 'form-select'

    class Meta:
        model = CFSWFBCharacteristic
        exclude = ['product', ]


class CellPhonesCharacteristicForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
            self.fields['color'].widget.attrs['class'] = 'form-select'
            self.fields['scratch_resistant_glass'].widget.attrs['class'] = 'form-select'
            self.fields['memory_card_slot'].widget.attrs['class'] = 'form-select'
            self.fields['access_internet'].widget.attrs['class'] = 'form-select'
            self.fields['camera'].widget.attrs['class'] = 'form-select'

    class Meta:
        model = CellPhonesCharacteristic
        exclude = ['product', ]


class SatellitePhonesCharacteristicForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
            self.fields['color'].widget.attrs['class'] = 'form-select'
            self.fields['operating_system'].widget.attrs['class'] = 'form-select'
            self.fields['sos_button'].widget.attrs['class'] = 'form-select'
            self.fields['touch_screen'].widget.attrs['class'] = 'form-select'

    class Meta:
        model = SatellitePhonesCharacteristic
        exclude = ['product', ]


class RAMCharacteristicForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
            self.fields['color'].widget.attrs['class'] = 'form-select'
            self.fields['memory_type'].widget.attrs['class'] = 'form-select'
            self.fields['ecc_support'].widget.attrs['class'] = 'form-select'
            self.fields['buffered'].widget.attrs['class'] = 'form-select'
            self.fields['low_profile'].widget.attrs['class'] = 'form-select'
            self.fields['timings'].widget.attrs['class'] = 'form-select'
            self.fields['radiator'].widget.attrs['class'] = 'form-select'

    class Meta:
        model = RAMCharacteristic
        exclude = ['product', 'product_name_copy', ]


class DiskDrivesCharacteristicForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
            self.fields['color'].widget.attrs['class'] = 'form-select'

    class Meta:
        model = DiskDrivesCharacteristic
        exclude = ['product', 'product_name_copy', ]


class VideoCardsCharacteristicForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
            self.fields['color'].widget.attrs['class'] = 'form-select'
            self.fields['vm_type'].widget.attrs['class'] = 'form-select'
            self.fields['mode_support'].widget.attrs['class'] = 'form-select'
            self.fields['lrh'].widget.attrs['class'] = 'form-select'

    class Meta:
        model = VideoCardsCharacteristic
        exclude = ['product', 'product_name_copy', ]


class ProcessorsCharacteristicForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
            self.fields['color'].widget.attrs['class'] = 'form-select'
            self.fields['integrated_graphics_core'].widget.attrs['class'] = 'form-select'

    class Meta:
        model = ProcessorsCharacteristic
        exclude = ['product', 'product_name_copy', ]


class SystemUnitsCharacteristicForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
            self.fields['color'].widget.attrs['class'] = 'form-select'
            self.fields['operating_system'].widget.attrs['class'] = 'form-select'
            self.fields['game'].widget.attrs['class'] = 'form-select'
            self.fields['ram_type'].widget.attrs['class'] = 'form-select'
            self.fields['drive_type'].widget.attrs['class'] = 'form-select'
            self.fields['optical_drive_type'].widget.attrs['class'] = 'form-select'
            self.fields['wifi'].widget.attrs['class'] = 'form-select'
            self.fields['card_reader'].widget.attrs['class'] = 'form-select'

    class Meta:
        model = SystemUnitsCharacteristic
        exclude = ['product', 'product_name_copy', ]


class MonoblocksCharacteristicForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
            self.fields['color'].widget.attrs['class'] = 'form-select'
            self.fields['operating_system'].widget.attrs['class'] = 'form-select'
            self.fields['touch_scr'].widget.attrs['class'] = 'form-select'
            self.fields['ram_type'].widget.attrs['class'] = 'form-select'
            self.fields['drive_type'].widget.attrs['class'] = 'form-select'
            self.fields['built_in_webcam'].widget.attrs['class'] = 'form-select'
            self.fields['built_in_speakers'].widget.attrs['class'] = 'form-select'
            self.fields['built_in_microphone'].widget.attrs['class'] = 'form-select'
            self.fields['wifi'].widget.attrs['class'] = 'form-select'
            self.fields['bluetooth'].widget.attrs['class'] = 'form-select'
            self.fields['optical_drive_type'].widget.attrs['class'] = 'form-select'
            self.fields['card_reader'].widget.attrs['class'] = 'form-select'

    class Meta:
        model = MonoblocksCharacteristic
        exclude = ['product', 'product_name_copy', ]