

QUESTION = [
    ("Да", "Да"),
    ("Нет", "Нет"),
    ("Есть", "Есть"),
    ("Отсутсвует", "Отсутсвует"),
]

OPERATING_SYSTEM = [
    ('Windows 11', 'Windows 11'),
    ('Windows 10', 'Windows 10'),
    ('Windows 7', 'Windows 7'),
    ('Microsoft Vista', 'Microsoft Vista'),
    ('Windows XP', 'Windows XP'),
    ('Microsoft Windows 3.1', 'Microsoft Windows 3.1'),
    ('Microsoft Windows 1.0', 'Microsoft Windows 1.0'),
    ('Mac OS X Leopard', 'Mac OS X Leopard'),
    ('iOS', 'iOS'),
    ('Android', 'Android'),
    ('Watch OS', 'Watch OS'),
    ('Android, iOS', 'Android, iOS'),
    ('Android, iOS, Windows', 'Android, iOS, Windows'),
    ('Firefox OS', 'Firefox OS'),
    ('Ubuntu', 'Ubuntu'),
    ('Macintosh OSX', 'Macintosh OSX'),
    ('Fedora', 'Fedora'),
    ('Xandros Linux', 'Xandros Linux'),
    ('Unix', 'Unix'),
    ('Linux', 'Linux'),
    ('Без ОС', 'Без ОС'),
]

MEMORY_TYPE = [
    ("DDR", "DDR"),
    ("DDR2", "DDR2"),
    ("DDR3", "DDR3"),
    ("DDR4", "DDR4"),
    ("DDR5", "DDR5"),
]

DISK_TYPE = [
    ("SSD", "SSD"),
    ("HDD", "HDD"),
]

VIDEO_MEMORY_TYPE = [
    ('GDDR', 'GDDR'),
    ('GDDR2', 'GDDR2'),
    ('GDDR3', 'GDDR3'),
    ('GDDR4', 'GDDR4'),
    ('GDDR5', 'GDDR5'),
    ('GDDR6', 'GDDR6'),
    ('HBM', 'HBM'),
    ('HBM2', 'HBM2'),
]
