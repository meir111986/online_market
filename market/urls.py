from django.urls import path

from market.views import (ProductListView, ProductDetailView, ProductCreateView, CategoryCreateProductListView,
                          FilterProductPrice, ProductUpdateView, ProductDeleteView, UserProductListView)

urlpatterns = [
    path('create/<int:catid>', ProductCreateView.as_view(), name='create_product'),
    path('<str:slug>/filter', FilterProductPrice.as_view(), name='filter'),
    path('all/', UserProductListView.as_view(), name='user_product_list'),
    path('<str:slug>/', ProductListView.as_view(), name='product_list'),
    path('<str:slug>/<int:pk>', ProductDetailView.as_view(), name='product_detail'),
    path('', CategoryCreateProductListView.as_view(), name='category_create_product_list'),
    path('<str:slug>/<int:pk>/edit', ProductUpdateView.as_view(), name='edit_product'),
    path('<str:slug>/<int:pk>/delete', ProductDeleteView.as_view(), name='delete_product'),

]
