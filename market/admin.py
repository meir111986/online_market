from django.contrib import admin
from django.utils.safestring import mark_safe
from .models import Category, Brand, Product, BatteryCharacteristic, CableCharacteristic, ProductAttachment, \
    ExternalBatteryCharacteristic, PhoneHoldersCharacteristic, SmartphoneDisplaysCharacteristic, \
    ChargingDeviceCharacteristic, ProtectiveFilmsGlassesCharacteristic, StickersPhonesCharacteristic, \
    SmartphoneCasesCharacteristic, ProtectiveFilmsGlassesSmartFitnessBraceletsCharacteristic, \
    StrapsSmartWatchesCharacteristic, SmartWatchCharacteristic, FitnessBraceletsCharacteristic, CFSWFBCharacteristic, \
    CellPhonesCharacteristic, SatellitePhonesCharacteristic, RAMCharacteristic, DiskDrivesCharacteristic, \
    VideoCardsCharacteristic, ProcessorsCharacteristic, SystemUnitsCharacteristic, MonoblocksCharacteristic
from django_mptt_admin.admin import DjangoMpttAdmin


class CategoryAdmin(DjangoMpttAdmin):
    prepopulated_fields = {"slug": ("title",)}


admin.site.register(Category, CategoryAdmin)


class ProductAdmin(admin.ModelAdmin):
    list_display = ('id', 'product_name', 'price', 'user', 'article', 'category', 'brand', 'get_last_image', 'creation_date', 'available')
    list_display_links = ('product_name',)
    list_editable = ('available', )
    list_filter = ('price', 'category', 'brand')
    search_fields = ('product_name', 'category')
    ordering = ('-id',)

    def get_last_image(self, obj):
        if ProductAttachment.objects.filter(product=obj):
            image = ProductAttachment.objects.filter(product=obj).last().image
            return mark_safe(f'<img src="{image.url}" width="60" alt="">')

    get_last_image.__name__ = "Картинка"


class CableCharacteristicAdmin(admin.ModelAdmin):
    list_display = ('id', 'connectors', 'type', 'length', 'color')
    list_display_links = ('connectors',)
    list_filter = ('type', 'color')
    search_fields = ('product', 'type')
    ordering = ('-id',)


class BatteryCharacteristicAdmin(admin.ModelAdmin):
    list_display = ('id', 'battery_capacity', 'type', 'voltage', 'additional_information', 'dimensions', 'weight', 'color')
    list_display_links = ('id', 'battery_capacity', 'type')
    list_filter = ('type', 'color')
    search_fields = ('product', 'type')
    ordering = ('-id',)


admin.site.register(Product, ProductAdmin)
admin.site.register(CableCharacteristic, CableCharacteristicAdmin)
admin.site.register(BatteryCharacteristic, BatteryCharacteristicAdmin)
admin.site.register(ExternalBatteryCharacteristic)
admin.site.register(PhoneHoldersCharacteristic)
admin.site.register(SmartphoneDisplaysCharacteristic)
admin.site.register(ChargingDeviceCharacteristic)
admin.site.register(ProtectiveFilmsGlassesCharacteristic)
admin.site.register(StickersPhonesCharacteristic)
admin.site.register(SmartphoneCasesCharacteristic)
admin.site.register(ProtectiveFilmsGlassesSmartFitnessBraceletsCharacteristic)
admin.site.register(StrapsSmartWatchesCharacteristic)
admin.site.register(SmartWatchCharacteristic)
admin.site.register(FitnessBraceletsCharacteristic)
admin.site.register(CFSWFBCharacteristic)
admin.site.register(CellPhonesCharacteristic)
admin.site.register(SatellitePhonesCharacteristic)
admin.site.register(RAMCharacteristic)
admin.site.register(DiskDrivesCharacteristic)
admin.site.register(VideoCardsCharacteristic)
admin.site.register(ProcessorsCharacteristic)
admin.site.register(SystemUnitsCharacteristic)
admin.site.register(MonoblocksCharacteristic)
admin.site.register(Brand)
admin.site.register(ProductAttachment)
