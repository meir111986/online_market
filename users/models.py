import os
from django.contrib.auth import get_user_model
from django.db import models
from phonenumber_field.modelfields import PhoneNumberField
from django.dispatch import receiver
from django.db.models.signals import post_save


class Profile(models.Model):
    user = models.OneToOneField(get_user_model(), related_name='user_profile', on_delete=models.CASCADE,
                                verbose_name='Пользователь')
    date_of_birth = models.DateField(blank=False, null=True, verbose_name='Дата рождения')
    gender_choice = [
        (0, 'Male'),
        (1, 'Female')
    ]
    gender = models.IntegerField(choices=gender_choice, null=True)
    profile_pic = models.ImageField(blank=True, null=True, upload_to='profile/pics', verbose_name='Аватар')
    phone_number = PhoneNumberField(blank=True, null=True, verbose_name="Номер телефона")
    information = models.TextField(blank=True, null=True, verbose_name='О себе')

    def __str__(self):
        return f'{self.user}\'s profile:'

    class Meta:
        verbose_name = 'Профиль'
        verbose_name_plural = 'Профили'


# Используется при удаление всего поста, удаляет картинку
@receiver(models.signals.pre_delete, sender=Profile)
def auto_delete_file(sender, instance, **kwargs):
    if instance.profile_pic:
        if os.path.isfile(instance.profile_pic.path):
            os.remove(instance.profile_pic.path)


# Удаление фото или картинки
# Используется при изменение картинки, удаляет старую картинку
@receiver(models.signals.pre_save, sender=Profile)
def auto_change_file(sender, instance, **kwargs):
    if not instance.pk:
        return False

    try:
        old_image = Profile.objects.get(pk=instance.pk).profile_pic
    except Profile.DoesNotExist:
        return False

    new_image = instance.profile_pic
    print('old_image', old_image)
    print(new_image)
    if old_image:
        if old_image != new_image:
            if os.path.isfile(old_image.path):
                os.remove(old_image.path)


@receiver(post_save, sender=get_user_model())
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=get_user_model())
def save_user_profile(sender, instance, **kwargs):
    print(instance, 12313)
    if not Profile.objects.filter(user=instance):
        Profile.objects.create(user=instance)
    else:
        instance.user_profile.save()
