from django.urls import path
from . import views
from django.contrib.auth.views import (PasswordChangeView,
                                       PasswordResetView,
                                       PasswordResetDoneView,
                                       PasswordResetConfirmView,
                                       PasswordResetCompleteView)

urlpatterns = [
    path('register', views.RegisterView.as_view(), name='register'),
    path('activate/(<uidb64>)/(<token>)/', views.activate, name='activate'),
    path('password_change', PasswordChangeView.as_view(
        template_name='registration/password_change.html',
        success_url='/'
    ), name='password_change'),
    path('password-reset/',
         PasswordResetView.as_view(template_name='registration/password_reset.html'),
         name='password_reset'),
    path('password-reset/done/',
         PasswordResetDoneView.as_view(template_name='registration/password_reset_done.html'),
         name='password_reset_done'),
    path('password-reset/confirm/(<uidb64>)/(<token>/',
         PasswordResetConfirmView.as_view(template_name='registration/password_reset_confirm.html'),
         name='password_reset_confirm'),
    path('password-reset/complate/',
         PasswordResetCompleteView.as_view(template_name='registration/password_reset_complete.html'),
         name='password_reset_complete'),
    path('login/', views.LoginView.as_view(), name='login'),
    path('profile/<int:pk>', views.ProfileDetailView.as_view(), name='profile_detail'),
    path('profile/edit', views.UserProfileEditView.as_view(), name='profile_edit'),
]
