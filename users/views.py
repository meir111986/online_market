from django.contrib.auth import authenticate, login, get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMessage
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.encoding import force_bytes, force_str
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.views import View
from django.views.generic import FormView, DetailView, UpdateView
from users.forms import SignUpForm, UserChangeForm, ProfileChangeForm
from users.models import Profile
from users.token import account_activation_token


class RegisterView(FormView):

    def get(self, request, *args, **kwargs):
        form = SignUpForm()
        context_next = {
            'next': request.GET.get("next")
        }
        return render(request, 'registration/register.html', context={'form': form, 'next': context_next['next']})

    def post(self, request, *args, **kwargs):
        form = SignUpForm(request.POST)
        next_page = request.GET.get("next")
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.save()
            Profile.objects.create(user=user)

            current_site = get_current_site(request)
            mail_subject = 'Activation link has been sent to your email id'
            message = render_to_string('registration/acc_active_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': account_activation_token.make_token(user),
            })
            to_email = form.cleaned_data.get('email')
            email = EmailMessage(
                mail_subject, message, to=[to_email]
            )
            email.send()
            if next_page is not None:
                return redirect(next_page)
            return HttpResponse('Please confirm your email address to complete the registration')
        return render(request, 'registration/register.html', {'form': form})


def activate(request, uidb64, token):
    try:
        uid = force_str(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        return HttpResponse('Thank you for your email confirmation. Now you can login your account.')
    else:
        return HttpResponse('Activation link is invalid!')


class LoginView(View):
    def get(self, request, *args, **kwargs):
        context = {
            'next': request.GET.get("next")
        }
        return render(request, 'registration/login.html', context)

    def post(self, request, *args, **kwargs):
        context = {}
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        next_page = request.GET.get("next")
        if user is not None:
            login(request, user)
            if next_page is not None:
                return redirect(next_page)
            return redirect('category_list')
        elif user is None:
            try:
                user_queryset = get_user_model().objects.all().filter(email__iexact=username)
                if user_queryset:
                    user = authenticate(username=user_queryset[0], password=password)
                    login(request, user)
                    if next_page is not None:
                        return redirect(next_page)
                    context['has_error'] = True
                    return redirect(reverse('category_list'))
            except AttributeError:
                context['has_error'] = True
        context['has_error'] = True
        return render(request, 'registration/login.html', context=context)


class ProfileDetailView(LoginRequiredMixin, DetailView):
    model = Profile
    context_object_name = 'profile'
    template_name = 'profile/profile_detail.html'


class UserProfileEditView(UpdateView):
    model = get_user_model()
    form_class = UserChangeForm
    template_name = 'profile/user_profile_update.html'
    context_object_name = 'user_obj'

    def get_object(self, queryset=None):
        return self.model.objects.get(id=self.request.user.id)

    def get_context_data(self, **kwargs):
        if 'profile_form' not in kwargs:
            kwargs['profile_form'] = self.get_profile_form()
        return super().get_context_data(**kwargs)

    def get_profile_form(self):
        form_kwargs = {'instance': self.object.user_profile}
        if self.request.method == 'POST':
            form_kwargs['data'] = self.request.POST
            form_kwargs['files'] = self.request.FILES
        return ProfileChangeForm(**form_kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        profile_form = self.get_profile_form()
        if form.is_valid() and profile_form.is_valid():
            return self.form_valid(form, profile_form)
        else:
            return self.form_invalid(form, profile_form)

    def form_valid(self, form, profile_form):
        response = super().form_valid(form)
        profile_form.save()
        return response

    def form_invalid(self, form, profile_form):
        context = self.get_context_data(form=form, profile_form=profile_form)
        return self.render_to_response(context)

    def get_success_url(self):
        return reverse('profile_detail', kwargs={'pk': self.request.user.user_profile.pk})
