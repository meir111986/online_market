from cart.cart import Cart
from users.models import Profile


def profiles(request):
    if request.user.is_authenticated:
        return {'profiles': Profile.objects.get(user=request.user.pk)}
    # if request.user.is_authenticated:
    #     return {'profiles': Profile.objects.all()}
    else:
        return {'profiles': Profile.objects.all()}


def cart(request):
    return {'cart': Cart(request)}
